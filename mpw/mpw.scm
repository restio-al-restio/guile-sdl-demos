;;; mpw

;; Copyright (C) 2022 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: mpw [--theme THEME] [--size BOARD-SIZE]
;;
;; THEME names a theme file in the data directory, one of:
;;   Hellsfire
;;   Linux
;;   Network
;;   Plumbing
;;   Simple
;;   scheme
;; or a compatible PNG file (you can make your own!).
;; If unspecified, the theme defaults to ‘Hellsfire’.
;;
;; Type ESC to quit, ‘m’ to switch modes (demo vs play).
;; Use arrow keys or number pad keys to pan view.
;; Type ‘keypad-0’ to toggle panning mode (view vs board).
;; Click left or right mouse buttons, or use mouse wheel to rotate pieces.
;;
;; Optional arg BOARD-SIZE is an odd number in the range [5,15]
;; (default BOARD-SIZE is 9).

;;; Code:

(define-module (sdl-demos mpw)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile))
  #:use-module ((guile-baux common) #:select (check-hv
                                              qop<-args
                                              fs))
  #:use-module ((ice-9 q) #:select (make-q
                                    q-empty?
                                    enq!
                                    deq!))
  #:use-module ((srfi srfi-1) #:select (append-map!
                                        car+cdr))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((srfi srfi-4) #:select (u8vector
                                        u8vector-ref
                                        u8vector-set!))
  #:use-module ((srfi srfi-9) #:select (define-record-type))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((sdl misc-utils) #:select (create-rgba-square
                                           rotate-square
                                           copy-surface
                                           rect<-surface
                                           exact-truncate
                                           exact-floor
                                           toroidal-panner/3p
                                           ignore-all-event-types-except))
  #:use-module ((sdl gfx) #:select (blit-rgba
                                    roto-zoom-surface
                                    multiply-pixel-alpha!
                                    make-fps-manager
                                    fps-manager-delay!))
  #:use-module ((sdl sdl) #:prefix SDL:))

;; state

(define HT #f)                          ; hash table: theme surfaces
(define FAR #f)                         ; board array: far ground
(define BSIZE #f)                       ; integer [5,15]
(define MID #f)                         ; board array: mid ground
(define SENDER #f)                      ; coords: #u8(R C)
(define SENDER-RECT #f)                 ; rectangle
(define STATS #f)                       ; alist
(define INTERESTING #f)                 ; hash table: ‘up’ pieces
(define NEWS #f)                        ; board array: link map
(define PAN-VIEW #f)                    ; boolean

;; procs

(define (fsym s . args)
  (string->symbol (apply format #f s args)))

(define TILE-EDGE-LENGTH 64)

(define (make-tile-rect r c)
  ;; note order change: r c => c r => x y
  (SDL:make-rect (* TILE-EDGE-LENGTH c)
                 (* TILE-EDGE-LENGTH r)
                 TILE-EDGE-LENGTH
                 TILE-EDGE-LENGTH))

(define BUTTONS '(left right ok-0 no mix tools blank ok-1))

(define STEPS-PER-QUADRANT 9)

(define FULL-LEN (* 4 STEPS-PER-QUADRANT))

(define STEMS '(up opp corn tee))

(define (qname stem)
  (fsym "q-~A" stem))

(define (fname stem)
  (fsym "f-~A" stem))

(define (load-theme filename)           ; => hash table

  (define (proper surface)
    ;; Q: This is very important!  Why?
    ;; A: Disabling Per-Surface Alpha allows Per-Pixel Alpha to work.
    (SDL:surface-alpha! surface #f)
    surface)

  (define (tname stem state)
    (fsym "~A-~A" stem state))

  (define turnables (append-map! (lambda (stem)
                                   (list (tname stem 0)
                                         (tname stem 1)))
                                 STEMS))

  (let* ((theme (proper (SDL:load-image filename)))
         (ht (make-hash-table))
         (rect (make-tile-rect 0 0)))

    (define (h! k v)
      (hashq-set! ht k v))

    (define (read-row! row tile-names)
      (SDL:rect:set-y! rect (* TILE-EDGE-LENGTH row))
      (do ((x 0 (+ x TILE-EDGE-LENGTH))
           (names tile-names (cdr names)))
          ((null? names))
        (SDL:rect:set-x! rect x)
        (h! (car names) (copy-surface theme rect))))

    (define (expand-stem! stem)
      (let ((quad (make-array #f 2 4))  ; [state][q]
            (full (make-vector FULL-LEN)))

        (define (named stem state)
          (hashq-ref ht (tname stem state)))

        (define (spread zero mult)
          ;; rv
          (lambda (i)
            (rotate-square zero (* mult i))))

        (define (aim array proc)
          (array-index-map! array proc)
          array)

        (h! (qname stem)
            (aim quad (let ((zero (vector (named stem 0)
                                          (named stem 1))))
                        (lambda (state q)
                          ((spread (vector-ref zero state)
                                   90)
                           q)))))

        (h! (fname stem)
            (aim full (spread (named stem 0)
                              (/ 90.0 STEPS-PER-QUADRANT))))))

    (read-row! 0 '(window cell sender rec-0 rec-1 lock win))
    (read-row! 1 turnables)
    (read-row! 2 BUTTONS)

    (for-each expand-stem! STEMS)

    ht))

(define (surf name)
  (hashq-ref HT name))

(define (set-board-size! dim)
  (let* ((diff (- dim BSIZE))
         (diff/2 (ash diff -1))
         (new (make-array #f dim dim)))

    (define fittingly
      (let ((lo diff/2)
            (hi (- dim 1 diff/2)))

        (define (proper coord)
          (if (<= lo coord hi)
              (- coord lo)
              (random 2)))

        ;; fittingly
        (lambda (r c)
          (array-ref FAR (proper r) (proper c)))))

    (define cropped
      (let ((lo (- diff/2)))
        ;; cropped
        (lambda (r c)
          (array-ref FAR (+ lo r) (+ lo c)))))

    (array-index-map! new (if (negative? diff)
                              cropped
                              fittingly))

    (set! FAR new)
    (set! BSIZE dim)))

(define (array-like-FAR)
  (apply make-array #f (array-shape FAR)))

(define-record-type piece
    (make-piece stem state q quad full)
    piece?
  (stem p.stem)
  (state p.state p.state!)
  (q p.q p.q!)
  (idx p.idx p.idx!)
  (quad p.quad)
  (full p.full))

(define (random-coord)
  (random BSIZE))

(define (u8v0 v) (u8vector-ref v 0))
(define (u8v1 v) (u8vector-ref v 1))

(define (make-sender-rect)
  (make-tile-rect (u8v0 SENDER)
                  (u8v1 SENDER)))

(define (blit-to-screen! surface rect)
  (SDL:blit-surface surface #f #f rect))

(define (refresh-scenery!)
  ;; see ‘prepare-proc!’ below
  (let ((rect (array-like-FAR)))

    (define (refresh! far mid rect)
      (blit-to-screen! (surf far) rect)
      (cond ((not mid))
            ((symbol? mid)
             ;; name
             (blit-to-screen! (surf mid) rect))
            (else
             ;; piece
             (let ((state (p.state mid)))
               (blit-to-screen!
                (cond ((p.idx mid)
                       => (lambda (idx)
                            (vector-ref (p.full mid) idx)))
                      (else
                       (array-ref (p.quad mid)
                                  state
                                  (p.q mid))))
                rect)
               (and (eq? 'up (p.stem mid))
                    (blit-to-screen! (surf (if (zero? state)
                                               'rec-0
                                               'rec-1))
                                     rect))))))

    ;; initialize ‘rect’
    (array-index-map! rect make-tile-rect)

    ;; refresh-scenery!
    (lambda ()
      (array-for-each refresh! FAR MID rect)
      (blit-to-screen! (surf 'sender) SENDER-RECT))))

(define (vrandom v)
  (vector-ref v (random (vector-length v))))

(define (show! ms)
  (SDL:flip)
  (SDL:delay ms))

(define (bmod n)
  (modulo n BSIZE))

(define-record-type ground
    (make-ground)
    ground?
  (cool g.cool g.cool!)                 ; endpoints
  (live g.live g.live!)
  (n g.n g.n!)
  (e g.e g.e!)
  (w g.w g.w!)
  (s g.s g.s!))

(define (g.cc g)
  (+ (if (g.n g) 1 0)
     (if (g.e g) 1 0)
     (if (g.w g) 1 0)
     (if (g.s g) 1 0)))

(define (symbol<-news news)
  (fsym "~A~A~A~A"
        (if (memq 'n news) 'n "")
        (if (memq 'e news) 'e "")
        (if (memq 'w news) 'w "")
        (if (memq 's news) 's "")))

(define TWO-WAYS
  (let ((ht (make-hash-table)))

    (define (note! stem q . news)
      (let ((piece-pov (cons stem q))
            (ground-pov (symbol<-news news)))
        (hash-set! ht piece-pov news)
        (hash-set! ht ground-pov piece-pov)))

    (note! 'up 0 'n)
    (note! 'up 1 'w)
    (note! 'up 2 's)
    (note! 'up 3 'e)

    (note! 'opp 0 'n       's)
    (note! 'opp 1    'e 'w   )
    (note! 'opp 2 'n       's)
    (note! 'opp 3    'e 'w   )

    (note! 'corn 0 'n 'e      )
    (note! 'corn 1 'n    'w   )
    (note! 'corn 2       'w 's)
    (note! 'corn 3    'e    's)

    (note! 'tee 0 'n 'e    's)
    (note! 'tee 1 'n 'e 'w   )
    (note! 'tee 2 'n    'w 's)
    (note! 'tee 3    'e 'w 's)

    ht))

(define (rethrow key args)
  (apply throw key args))

(define (true-list . args)
  (delq #f args))

(define (generate!)
  ;; see ‘prepare-proc!’ below
  (let ((sender-r (u8v0 SENDER))
        (sender-c (u8v1 SENDER))
        (connectome (array-like-FAR)))

    (define (random-direction-not . dirs)
      (let loop ()
        (let ((try (vrandom #(n e w s))))
          (if (memq try dirs)
              (loop)
              try))))

    (define (jam! r c)
      (let ((g (array-ref connectome r c)))
        (g.cool! g #t)))

    (define (live! r c)
      (let ((g (array-ref connectome r c)))
        (or (= 1 (g.cc g))
            (g.live! g #t))))

    (define (status r c)
      (let* ((g (array-ref connectome r c))
             (cool (g.cool g))
             (live (g.live g)))
        (case (g.cc g)
          ((0) (cond ((not cool)
                      'free)
                     ((and (not live)
                           (= sender-r r)
                           (= sender-c c))
                      'live)
                     (else
                      'reserved)))
          ((1) 'reserved)
          ((2) (if live
                   'live
                   'partial))
          ((3) 'full))))

    (define (new-place)
      (let loop ((count 27))
        (let ((try (u8vector (random-coord)
                             (random-coord))))
          (if (zero? count)
              (throw 'hands-in-air 'new-place)
              (case (status (u8v0 try) (u8v1 try))
                ((free) try)
                (else (loop (1- count))))))))

    (define (neighbor r c dir)
      (let ((r (case dir
                 ((n) (bmod (1- r)))
                 ((s) (bmod (1+ r)))
                 (else r)))
            (c (case dir
                 ((e) (bmod (1+ c)))
                 ((w) (bmod (1- c)))
                 (else c))))
        (values r c
                (case dir
                  ((n) 's)
                  ((e) 'w)
                  ((w) 'e)
                  ((s) 'n))
                (status r c))))

    (define (setter dir)
      (case dir
        ((n) g.n!)
        ((e) g.e!)
        ((w) g.w!)
        ((s) g.s!)))

    (define (place-and-route!)
      (let* ((u8vec (new-place))
             (r (u8v0 u8vec))
             (c (u8v1 u8vec))
             (maybe (list)))

        (define (link! r c dir)
          (set! maybe (cons (cons* dir r c) maybe))
          (let ((g (array-ref connectome r c)))
            ((setter dir) g #t)))

        (define (connect! r c dir nr nc opp)
          (link! r c dir)
          (link! nr nc opp))

        (define (unlink! k)
          (let ((g (array-ref connectome (cadr k) (cddr k))))
            ((setter (car k)) g #f)))

        (define (for-each-maybe proc)
          (for-each proc maybe))

        (define (attempt)
          (jam! r c)
          (let loop ((avoid '())
                     (r r) (c c)
                     (dir (random-direction-not)))
            (let-values (((nr nc opp s) (neighbor r c dir)))
              (case s
                ((live)
                 (connect! r c dir nr nc opp)
                 (for-each-maybe
                  (lambda (k)
                    (let ((pair (cdr k)))
                      (live! (car pair)
                             (cdr pair))))))
                ((free)
                 (connect! r c dir nr nc opp)
                 (loop '()
                       nr nc
                       (random-direction-not opp)))
                (else
                 (cond ((= 3 (length avoid))
                        (for-each-maybe unlink!)
                        (set! maybe (list))
                        (throw 'hands-in-air 'boxed-in)))
                 (let ((no (cons dir avoid)))
                   (loop no
                         r c
                         (apply random-direction-not no))))))))

        (let loop ((ok #f) (patience 0))
          (catch #t (lambda ()
                      (attempt)
                      (set! ok #t))
                 (lambda (key . args)
                   (and (= 9 patience)
                        (rethrow key args))))
          (or ok (loop ok (1+ patience))))))

    (define quad<-
      (let ((alist (map (lambda (stem)
                          (cons stem (surf (qname stem))))
                        STEMS)))
        ;; quad<-
        (lambda (stem)
          (assq-ref alist stem))))

    (define full<-
      (let ((alist (map (lambda (stem)
                          (cons stem (surf (fname stem))))
                        STEMS)))
        ;; full<-
        (lambda (stem)
          (assq-ref alist stem))))

    (define (piece<-ground g)

      (define (stem+q)
        (car+cdr (hash-ref TWO-WAYS
                           (symbol<-news
                            (true-list
                             (and (g.n g) 'n)
                             (and (g.e g) 'e)
                             (and (g.w g) 'w)
                             (and (g.s g) 's))))))

      (and (positive? (g.cc g))
           (let-values (((stem q) (stem+q)))
             (make-piece stem 0 q
                         (quad<- stem)
                         (full<- stem)))))

    ;; generate!
    (lambda ()                          ; => stats (alist)
      (array-map! connectome make-ground)
      (jam! sender-r sender-c)
      (catch #t (lambda ()
                  (let loop ()
                    (place-and-route!)
                    (loop)))
             (lambda (key . args)
               (case key
                 ((hands-in-air) args)  ; nop
                 (else (rethrow key args)))))
      (array-map! MID piece<-ground connectome)
      (let ((count (make-object-property)))

        (define (cur cat)
          (or (count cat)
              0))

        (array-for-each (lambda (what)
                          (let ((cat (if what
                                         (p.stem what)
                                         'free)))
                            (set! (count cat)
                                  (1+ (cur cat)))))
                        MID)
        (map (lambda (cat)
               (cons cat (cur cat)))
             (append STEMS '(free)))))))

(define (read-news!)

  (define (news<-piece piece)
    (hash-ref TWO-WAYS (cons (p.stem piece)
                             (p.q    piece))))

  (array-map! NEWS (lambda (what)
                     (if (piece? what)
                         (news<-piece what)
                         '()))
              MID))

(define (set-state! state)
  ;; rv
  (lambda (what)
    (and (piece? what)
         (p.state! what state))))

(define (share! discreet)               ; => count of INTERESTING live

  (define (check dir r c)
    (let ((r (bmod r))
          (c (bmod c)))
      (and (memq dir (array-ref NEWS r c))
           (u8vector r c))))

  (define (neighbors r c)
    (let ((me (array-ref NEWS r c)))
      (true-list (and (memq 'n me) (check 's (1- r)     c))
                 (and (memq 'e me) (check 'w     r  (1+ c)))
                 (and (memq 'w me) (check 'e     r  (1- c)))
                 (and (memq 's me) (check 'n (1+ r)     c)))))

  (let ((connect! (set-state! 1))
        (seen (make-hash-table))
        (todo (make-q)))

    (define (visit v)
      (or (hash-ref seen v)
          (enq! todo v))
      (hash-set! seen v #t))

    (visit SENDER)
    (let loop ((eyebrows-raised 0))
      (if (q-empty? todo)
          (begin
            (or discreet
                (begin
                  (refresh-scenery!)
                  (SDL:flip)))
            ;; rv
            eyebrows-raised)
          (let* ((next (deq! todo))
                 (r (u8v0 next))
                 (c (u8v1 next))
                 (piece (array-ref MID r c)))
            (connect! piece)
            (for-each visit (neighbors r c))
            (loop ((if (hashq-ref INTERESTING piece)
                       1+
                       identity)
                   eyebrows-raised)))))))

(define FG-EDGE-LENGTH 32)

(define handle-any-input
  (let ((ev (SDL:make-event)))

    (define (grid-coord aspect)
      (exact-floor (/ (aspect ev) TILE-EDGE-LENGTH)))

    ;; handle-any-input
    (lambda wait
      (and ((if (and (pair? wait)
                     (car wait))
                SDL:wait-event
                SDL:poll-event)
            ev)
           (case (SDL:event:type ev)
             ((mouse-button-down)
              (throw 'button-down
                     (SDL:event:button:button ev)
                     ;; x y => y x => r c
                     (grid-coord SDL:event:button:y)
                     (grid-coord SDL:event:button:x)))
             ((key-down)
              (let ((sym (SDL:event:key:keysym:sym ev)))
                (and (eq? 'escape sym)
                     (throw 'quit))
                (throw 'key-down sym))))))))

(define-record-type manifestation
    (make-manifestation)
    manifestation?
  (x m.x m.x!)
  (y m.y m.y!)
  (vx m.vx m.vx!)
  (vy m.vy m.vy!)
  (idx m.idx m.idx!))

(define (hooray!)
  ;; see ‘prepare-proc!’ below
  (let* ((fps (make-fps-manager 20))
         (pi (* 2 (acos 0.0)))
         (family 3)
         (throng (* family (assq-ref STATS 'up)))
         (step-count 42))

    (define (svec)
      (make-vector step-count))

    (define (make-put win)
      (let ((surface (create-rgba-square FG-EDGE-LENGTH)))
        (blit-rgba win #f surface #f)
        surface))

    (define (fg-step put fraction)

      (define (dim! surface)
        (let ((c (exact-truncate (* 256 fraction))))
          (multiply-pixel-alpha! surface (- 255 c))
          surface))

      (define (s-curve x)
        (define beta (exp 0.8))
        (/ 1.0
           (+ 1.0 (expt (/ x
                           (- 1.0 x))
                        (- beta)))))

      (define (shift-up x)
        (+ 0.1 (* 0.9 x)))

      (define (size/rotate! surface)
        (roto-zoom-surface surface
                           (+ 21.5 (* -42.0 fraction))
                           (* 4.20 (shift-up (s-curve fraction)))
                           #t))

      (dim! (size/rotate! put)))

    (define (/2 n)
      (ash n -1))

    (define (half aspect)
      ;; rv
      (lambda (surface)
        (/2 (aspect surface))))

    (define (tvec)
      (make-vector throng))

    (define given (list->vector
                   (hash-fold (lambda (k v acc)
                                (append (make-list family v)
                                        acc))
                              '()
                              INTERESTING)))

    (define (random-c sel)
      ;; rv
      (lambda (given)
        (+ (* TILE-EDGE-LENGTH (sel given))
           FG-EDGE-LENGTH
           (random 9)
           -4)))

    (define (random-velocity)

      (define (random-magnitude)
        (+ 3.33 (random 4.20)))

      (define (random-angle)
        (+ (/ pi 4)
           (random (/ pi 2))))

      (make-polar (random-magnitude)
                  (random-angle)))

    (define (random-gravity)
      (random 0.222))

    (define (bump! orig delta)
      (array-map! orig + orig delta))

    (define (place-fg! fg w/2 h/2 dst)
      ;; rv
      (lambda (x y)
        (SDL:rect:set-x! dst (exact-floor (- x w/2)))
        (SDL:rect:set-y! dst (exact-floor (- y h/2)))
        (blit-to-screen! fg dst)))

    (define (random-pause)
      (- (random 27)))

    (define (random-manifestation! m given)
      (let ((vel (random-velocity)))
        (m.x!   m ((random-c cdr) given))
        (m.y!   m ((random-c car) given))
        (m.vx!  m (real-part vel))
        (m.vy!  m (- (imag-part vel)))
        (m.idx! m (random-pause))))

    (define (random-manifestation given)
      (let ((m (make-manifestation)))
        (random-manifestation! m given)
        m))

    (let ((fg (svec))
          (w/2 (svec))
          (h/2 (svec))
          (m (tvec))                    ; manifestation
          (vel (tvec))                  ; velocity
          (gravity (tvec))
          (dst (svec))
          (put (svec)))

      (array-index-map! fg (lambda (i)
                             (fg-step (make-put (surf 'win))
                                      (/ i step-count))))
      (array-map! w/2 (half SDL:surface:w) fg)
      (array-map! h/2 (half SDL:surface:h) fg)
      (array-map! dst rect<-surface fg)
      (array-map! put place-fg! fg w/2 h/2 dst)

      ;; hooray!
      (lambda verily
        (array-map! m random-manifestation given)
        (array-map! gravity random-gravity)
        (let ((end (and (null? verily)
                        (+ 4200 (random 4200) (SDL:get-ticks)))))
          (let loop ()
            (and end
                 (< end (SDL:get-ticks))
                 (throw 'celebration-done))
            (handle-any-input)
            (refresh-scenery!)
            (array-for-each
             (lambda (m gravity given)
               (or (negative? (m.idx m))
                   (let ((put (vector-ref put (m.idx m))))
                     (m.x!  m (+ (m.x  m) (m.vx m)))
                     (m.y!  m (+ (m.y  m) (m.vy m)))
                     (m.vy! m (+ (m.vy m) gravity))
                     (put (m.x m) (m.y m))))
               (m.idx! m (1+ (m.idx m)))
               (and (= step-count (m.idx m))
                    (random-manifestation! m given)))
             m gravity given)
            (SDL:flip)
            (fps-manager-delay! fps)
            (loop)))))))

(define (change-scenery-slightly!)

  (define random-button
    (let ((v (list->vector BUTTONS)))
      ;; random-button
      (lambda ()
        (vrandom v))))

  (define (new-place)
    (let loop ((count 27))
      (let ((try (u8vector (random-coord)
                           (random-coord))))
        (cond ((zero? count)
               (array-map! MID (lambda (cur)
                                 (and (piece? cur)
                                      cur))
                           MID)
               (loop 27))
              ((array-ref MID (u8v0 try) (u8v1 try))
               (loop (1- count)))
              (else
               try)))))

  (let* ((u8vec (new-place))
         (r (u8v0 u8vec))
         (c (u8v1 u8vec)))

    (define (arc! a name)
      (array-set! a name r c))

    (arc! FAR (if (eq? 'cell (array-ref FAR r c))
                  'window
                  'cell))
    (arc! MID (random-button))))

(define (turn!)
  ;; see ‘prepare-proc!’ below
  (let ((fps (make-fps-manager 42))
        (sign  (array-like-FAR))
        (count (array-like-FAR))
        (todo  (array-like-FAR)))

    (define (random-sign)
      (if (zero? (random 2))
          1+
          1-))

    (define (random-count)
      (random 3))

    (define (randomish-count count)
      (if (zero? count)
          count
          (max 1 (random-count))))

    (define (clamped n)
      (modulo n FULL-LEN))

    (define (idx<-q q)
       (* STEPS-PER-QUADRANT q))

    (define (qmod n)
      (modulo n 4))

    (define (one mid sign count)
      (and (positive? count)
           (piece? mid)
           (let* ((q    (p.q mid))
                  (nq   (qmod (+ (* count (sign 0)) q)))
                  (nidx (idx<-q nq)))
             ;; init
             (or (p.idx  mid)
                 (p.idx! mid (idx<-q q)))
             ;; rv
             (lambda (command)
               (cond ((not (p.idx mid)))
                     ((eq? 'stop command)
                      (p.q! mid (qmod
                                 ;; include ‘sign’ to approximate momentum
                                 (sign (inexact->exact
                                        (round (/ (p.idx mid)
                                                  STEPS-PER-QUADRANT)))))))
                     ((= (p.idx mid) nidx)
                      (p.q! mid nq)
                      (p.idx! mid #f))
                     (else
                      (p.idx! mid (clamped (sign (p.idx mid))))))))))

    (define (do-something! command)
      (array-for-each (lambda (proc)
                        (and proc (proc command)))
                      todo))

    ;; turn!
    (lambda (restart . specifically)

      (define sugar #f)

      (define short-and-sweet (pair? specifically))

      (array-for-each (set-state! 0) MID)
      (if short-and-sweet
          (let ((r (cadr specifically))
                (c (caddr specifically)))

            (define (arc! array value)
              (array-set! array value r c))

            (set! sugar (array-ref MID r c))

            (arc! MID #f)
            (read-news!)
            (share! #t)
            (arc! MID sugar)
            (arc! sign (case (car specifically)
                         ((left wheel-down) 1+)
                         ((right wheel-up) 1-)
                         (else (random-sign))))
            (array-fill! count 0)
            (arc! count 1))
          (begin
            (array-map! sign random-sign)
            (if restart
                (array-map! count randomish-count count)
                (array-map! count random-count))))
      (array-map! todo one MID sign count)
      (let loop ()

        (define still-going?
          (if short-and-sweet
              (lambda ()
                (p.idx sugar))
              (lambda ()
                (catch #t (lambda ()
                            (array-for-each (lambda (mid)
                                              (and (piece? mid)
                                                   (p.idx mid)
                                                   (throw 'still-going)))
                                            MID)
                            #f)
                       (lambda (key . args)
                         (or (eq? 'still-going key)
                             (rethrow key args)))))))

        (cond ((still-going?)
               (catch #t handle-any-input
                      (lambda (key . args)
                        (apply bye! key args)
                        (case key
                          ((button-down)
                           (do-something! 'stop)
                           (rethrow key args)))))
               (do-something! 'go)
               (refresh-scenery!)
               (SDL:flip)
               (fps-manager-delay! fps)
               (loop))))
      (refresh-scenery!))))

(define (bye! key . args)
  (and (eq? 'quit key)
       (exit (SDL:quit))))

(define (pan! dr dc)

  ;; PAN-VIEW #f
  ;;
  ;; Positive delta means what changes position from P to P+1.
  ;; Negative delta means what changes position from P to P-1.
  ;; (i.e. The position changes positively/negatively.)
  ;;
  ;;   -,-  -,0  -,+
  ;;   0,-  0,0  0,+       position
  ;;   +,-  +,0  +,+

  ;; PAN-VIEW #t
  ;;
  ;; Positive delta means what was in position P is now found in P-1.
  ;; Negative delta means what was in position P is now found in P+1.
  ;; (i.e. The "view" changes positively/negatively.)
  ;;
  ;;   -,-  -,0  -,+
  ;;   0,-  0,0  0,+       view
  ;;   +,-  +,0  +,+
  ;;

  (define (another src)
    (let ((dst (array-like-FAR)))
      (array-copy! src dst)
      dst))

  (define (new-foo invert delta)
    ;; rv
    (lambda (old)
      (bmod (+ (if (if PAN-VIEW
                       invert
                       (not invert))
                   (- delta)
                   delta)
               old))))

  (define new-r (new-foo #t dr))
  (define new-c (new-foo #t dc))

  (define (shift! array)
    (let ((new-view-r (new-foo #f dr))
          (new-view-c (new-foo #f dc))
          (was (another array)))
      (array-index-map! array (lambda (r c)
                                (array-ref was
                                           (new-view-r r)
                                           (new-view-c c))))))

  (u8vector-set! SENDER 0 (new-r (u8v0 SENDER)))
  (u8vector-set! SENDER 1 (new-c (u8v1 SENDER)))
  (set! SENDER-RECT (make-sender-rect))

  (shift! FAR)
  (shift! MID)

  (hash-for-each (lambda (k v)
                   (let ((r (car v))
                         (c (cdr v)))
                     (set-car! v (new-r r))
                     (set-cdr! v (new-c c))))
                 INTERESTING)

  ;; the Whole Point :-D
  (let ((screen (SDL:get-video-surface)))

    (define (wp dx dy)
      (let-values (((init! next! done!) (toroidal-panner/3p
                                         screen
                                         dx
                                         dy
                                         #f #t)))
        ;; rv
        (lambda (count)
          (init! count)
          (let loop ()
            (let ((continue? (next!)))
              (usleep 420)
              (and continue? (loop))))
          (done!))))

    ;; dr dc => dc dr => dx dy
    (define wp-view (wp    dc     dr))
    (define wp-posn (wp (- dc) (- dr)))

    ;; TODO: Use derivative of ‘s-curve’, scaled to TILE-EDGE-LENGTH.
    ;;       More precisely, the integral of the the derivative of the
    ;;       ‘s-curve’ (i.e., the ‘s-curve’ itself!) from beginning
    ;;       to end must sum up to TILE-EDGE-LENGTH.  (F*** ‘beta’!)
    ;;       (Where have your mad calculus skillz gone, dude?)
    (define ramp (list 1 1 1 2 3 6 8 10 10 8 6 3 2 1 1 1))
    ;; i.e., (apply + ramp) => TILE-EDGE-LENGTH
    ;; The result might be the same (or very similar), but
    ;; it's the *process* that counts.  "Only math is real."

    (for-each (if PAN-VIEW
                  wp-view
                  wp-posn)
              ramp))

  (refresh-scenery!)
  (SDL:flip))

(define (do-it!)
  (let loop ((mode 'demo))

    (define (dispatch key . args)
      (and (eq? 'quit key)
           (exit (SDL:quit)))
      (case key
        ((key-down)
         (refresh-scenery!)
         (SDL:flip)
         (let ((keysym (car args)))
           (case keysym
             ((kp-0)       (set! PAN-VIEW (not PAN-VIEW)))
             ((kp-7)       (pan! -1 -1))
             ((kp-8 up)    (pan! -1  0))
             ((kp-9)       (pan! -1  1))
             ((kp-4 left)  (pan!  0 -1))
             ((kp-6 right) (pan!  0  1))
             ((kp-1)       (pan!  1 -1))
             ((kp-2 down)  (pan!  1  0))
             ((kp-3)       (pan!  1  1)))
           (loop (case keysym
                   ((m) (if (eq? 'demo mode)
                            'play
                            'demo))
                   (else mode)))))
        ((button-down)
         args)
        ((celebration-done)
         args)
        (else
         (rethrow key args))))

    (define (move! . specifically)
      (let loop ((restart #f))
        (catch #t (lambda ()
                    (apply turn! restart specifically)
                    (set! restart #f))
               (lambda (key . args)
                 (apply dispatch key args)
                 (set! restart #t)))
        (and restart (loop #t))))

    (define (mix!)
      (move!))

    (define (wait-for-move)

      (define (makes-sense r c)
        (piece? (array-ref MID r c)))

      (let ((args (catch #t (lambda () (handle-any-input #t)) dispatch)))
        (and (apply makes-sense (cdr args))
             (apply move! args))))

    (case mode
      ((play) (wait-for-move))
      ((demo) (mix!))
      ((hooray) #t))
    (read-news!)
    (and (= (assq-ref STATS 'up) (share! #f))
         (set! mode 'hooray))
    (case mode
      ((demo) (catch #t hooray! dispatch))
      ((hooray) (catch #t (lambda () (hooray! #t)) dispatch)))
    (or (zero? (assq-ref STATS 'free))
        (change-scenery-slightly!))
    (loop mode)))

;; The original conception of some of these procedures was that they be
;; intermingled w/ top-level state assignments (e.g., ‘FAR’) and evaluated
;; in sequence (as a script).  This way, they could capture some state (or
;; transformed state) in their definition.
;;
;; Now that the state is all bunched together at the top, and assigned only
;; in ‘main/qop’, these original procedures have been converted to thunks
;; that only finish their "initialization" when called for the first time.
;; Furthermore, the first call returns the new initialized closure.  Thus,
;; the protocol is that to be useful, they all need to undergo:
;;
;;  (set! PROC (PROC))
;;
;; somewhere in ‘main/qop’ (properly intermingled w/ the state assignments).
;; This macro is an attempt to abstract this protocol (slightly :-D).
;;
(define-macro (prepare-proc! proc)
  `(set! ,proc (,proc)))

(define (main/qop qop)
  (set! *random-state* (seed->random-state (let ((now (gettimeofday)))
                                             (* (car now) (cdr now)))))
  (SDL:init '(video event-thread))

  (let-values (((cap mem fmt) (SDL:video-cmf)))
    (and (memq 'wm-available cap)
         (SDL:set-caption "mini-pipewalker")))

  (set! HT (load-theme
            (let ((fn (lambda (theme)
                        (datafile (fs "data/~A-top.png" theme)))))
              (or (qop 'theme
                       (lambda (theme)
                         (or (and (file-exists? theme)
                                  theme)
                             (let ((full (fn theme)))
                               (and (file-exists? full)
                                    full)))))
                  (fn "Hellsfire")))))

  (set! FAR
        #2(( cell  window window  cell  window  cell  window window  cell )
           (window window window  cell  window  cell  window window window)
           (window window  cell  window  cell  window  cell  window window)
           ( cell   cell  window  cell  window  cell  window  cell   cell )
           (window window  cell  window  cell  window  cell  window window)
           ( cell   cell  window  cell  window  cell  window  cell   cell )
           (window window  cell  window  cell  window  cell  window window)
           (window window window  cell  window  cell  window window window)
           ( cell  window window  cell  window  cell  window window  cell )))

  (set! BSIZE (car (array-dimensions FAR)))

  (set-board-size! (or (and-let* ((dim (qop 'size string->number))
                                  ((odd? dim))
                                  ((exact? dim))
                                  ((<= 5 dim 15)))
                         dim)
                       9))

  (SDL:set-video-mode (* BSIZE TILE-EDGE-LENGTH)
                      (* BSIZE TILE-EDGE-LENGTH)
                      32)

  (set! MID (array-like-FAR))

  (set! SENDER (u8vector (random-coord)
                         (random-coord)))

  (set! SENDER-RECT (make-sender-rect))

  (prepare-proc! generate!)

  (set! STATS
        (let ((interesting-enough (if (> 9 BSIZE)
                                      3
                                      9)))
          (let loop ()
            (let ((stats (generate!)))
              (if (< interesting-enough (assq-ref stats 'up))
                  stats
                  (loop))))))

  (set! INTERESTING
        (let ((ht (make-hash-table)))

          (array-index-map!
           (array-like-FAR)
           (lambda (r c)
             (and=> (array-ref MID r c)
                    (lambda (what)
                      (and (piece? what)
                           (eq? 'up (p.stem what))
                           (hashq-set! ht what (cons r c)))))))

          ht))

  (set! NEWS (array-like-FAR))

  (prepare-proc! refresh-scenery!)
  (prepare-proc! hooray!)
  (prepare-proc! turn!)

  (ignore-all-event-types-except 'key-down 'mouse-button-down)
  (do-it!))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.0")
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((theme (single-char #\t) (value #t))
           (size (single-char #\s) (value #t))))))

;;; mpw ends here
