;;; mars-rescue --- test a solution to the mars-rescue problem

;; Copyright (C) 2005, 2007, 2011, 2012, 2013, 2021 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: mars-rescue [--solution NAME] MAPFILE
;;
;; Load MAPFILE and simulate using solution MAPFILE.BEST, or MAPFILE.NAME
;; if given option ‘--solution’ (‘-s’ for short).  Display intermediate
;; simulation steps.  If the goal is achieved, display the line:
;;
;;   valid solution, steps: STEPS, fuel usage: FUEL
;;
;; w/ STEPS and FUEL both numbers.  Otherwise, display "invalid solution".
;; MAPFILE has the following format: the first line is number of rows R;
;; the second line is number of columns C; the rest of the file is a direct
;; character map (R lines of C columns + newline) w/ the following meanings:
;;
;;   #\space  space
;;   #\O      start
;;   #\+      person not to be missed (rescuee)
;;   #\#      stone
;;
;; A solution file is a single line of digits in the range 0 to 7 inclusive,
;; encoding a bitmask of left-jet, bottom-jet and right-jet in bit positions
;; 0, 1 and 2, respectively.

;;; Code:

(define-module (mars-rescue)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile decry))
  #:use-module ((guile-baux common) #:select (fs fso check-hv qop<-args))
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((ice-9 receive) #:select (receive))
  #:use-module ((srfi srfi-9) #:select (define-record-type))
  #:use-module ((srfi srfi-17) #:select (getter-with-setter))
  #:use-module ((sdl sdl) #:prefix SDL:)
  #:use-module ((sdl gfx) #:prefix GFX:)
  #:use-module ((sdl misc-utils) #:select (ignore-all-event-types-except))
  #:use-module ((sdl simple) #:select (simple-canvas)))

(define eye-the-door-and-leave-maybe!
  (let ((ev (SDL:make-event)))
    ;; eye-the-door-and-leave-maybe!
    (lambda ()
      (and (SDL:poll-event ev)
           (eq? 'key-down (SDL:event:type ev))
           (eq? 'escape (SDL:event:key:keysym:sym ev))
           (quit)))))

(define START #\O)
(define PNTBM #\+)                      ; "person not to be missed!"
(define SPACE #\space)
(define STONE #\#)

(define ship (make-object-property))
(define person-picked-up? (make-object-property))
(define goal-achieved? (make-object-property))

(define-record-type :ship
  (new-ship x y vx vy)
  ship?
  ;; What's w/ the funky "*" in these getter names?
  ;; Well, later versions of Guile seem to have a
  ;; problem w/ SRFI 17 usage of the form:
  ;;  (define FOO (getter-with-setter FOO ...))
  ;; So, the "*" names serves to disambiguate.
  ;; Ah, the joys of progress!
  (x ship*x ship-x!)
  (y ship*y ship-y!)
  (vx ship*vx ship-vx!)
  (vy ship*vy ship-vy!))

(define (new-map init)                  ; => array w/ properties
  (receive (w h next-line)
      (cond ((pair? init)
             (values
              (string-length (car init))
              (length init)
              (lambda () (let ((line (car init)))
                           (set! init (cdr init))
                           line))))
            ((string? init)
             (or (file-exists? init)
                 (decry "no such file: ~A" init))
             (let ((p (open-input-file init)))
               (values
                (string->number (read-line p))
                (string->number (read-line p))
                (lambda () (read-line p)))))
            (else
             (decry "unrecognized init: ~S" init)))
    (let ((ra (make-array #f h w)))
      (do ((y 0 (1+ y)))
          ((= h y) ra)                  ; rv
        (let ((line (next-line)))
          (do ((x 0 (1+ x)))
              ((= x w))
            (let ((c (string-ref line x)))
              (cond ((char=? START c)
                     (set! (ship ra) (new-ship (* 10 x) (* 10 y) 0 0))))
              (array-set! ra c y x))))))))

(define (display-map ra)
  (let ((w (cadr (array-dimensions ra)))
        (col 0))
    (array-for-each (lambda (k)
                      (display k)
                      (set! col (1+ col))
                      (cond ((= w col) (set! col 0) (newline))))
                    ra)))

(define ship-x  (getter-with-setter ship*x  ship-x!))
(define ship-y  (getter-with-setter ship*y  ship-y!))
(define ship-vx (getter-with-setter ship*vx ship-vx!))
(define ship-vy (getter-with-setter ship*vy ship-vy!))

(define (inc! gs obj value)
  (set! (gs obj) (+ (gs obj) value)))

(define (q/2! gs obj)
  (set! (gs obj) (quotient (gs obj) 2)))

(define (hit? ra x y cell-type)
  (define (test-1 x y)
    (let ((mx (quotient x 10))
          (my (quotient y 10)))
      (char=? cell-type
              (if (array-in-bounds? ra my mx)
                  (array-ref ra my mx)
                  STONE))))
  (let ((right  (+ 9 x))
        (bottom (+ 9 y)))
    (or (test-1     x y)
        (test-1 right y)
        (test-1     x bottom)
        (test-1 right bottom))))

(define *vv* 2)

(define (update-ship! ra ship L? B? R?)
  (define (bound-10! ?!)
    (and (> -10 (?! ship)) (set! (?! ship) -10))
    (and (<  10 (?! ship)) (set! (?! ship)  10)))
  (and L? (inc! ship-vx ship (- *vv*)))
  (and B? (inc! ship-vy ship (- *vv*)))
  (and R? (inc! ship-vx ship (+ *vv*)))
  (and #t (inc! ship-vy ship (+ 1)))
  (bound-10! ship-vx)
  (bound-10! ship-vy)
  (let loop ()
    (or (= 0 (ship-vx ship) (ship-vy ship))
        (let ((new-x (+ (ship-x ship) (ship-vx ship)))
              (new-y (+ (ship-y ship) (ship-vy ship))))
          (cond ((hit? ra new-x new-y STONE)
                 (q/2! ship-vx ship)
                 (q/2! ship-vy ship)
                 (loop))
                (else
                 (set! (ship-x ship) new-x)
                 (set! (ship-y ship) new-y)))))))

(define (update! ra L? B? R?)
  (let ((ship (ship ra)))
    (update-ship! ra ship L? B? R?)
    (let ((sx (ship-x ship))
          (sy (ship-y ship)))
      (and (hit? ra sx sy PNTBM)
           (set! (person-picked-up? ra) #t))
      (and (person-picked-up? ra)
           (hit? ra sx sy START)
           (set! (goal-achieved? ra) #t)))))

(define (run-solution! init solution . display/report)
  (let* ((ra (new-map init))
         (ship (ship ra))
         (show! (if (null? display/report)
                    display-map
                    (caar display/report)))
         (spew! (if (null? display/report)
                    (lambda (yay! L? B? R? s x y vx vy)
                      (fso "step: ~A, x: ~A, y: ~A, speed-x: ~A, speed-y: ~A~%"
                           s x y vx vy))
                    (cdar display/report))))
    (show! ra)
    (let loop ((ls (let ((zero (char->integer #\0)))
                     (map (lambda (c)
                            (- (char->integer c) zero))
                          (string->list solution))))
               (step 0)
               (fuel 0))
      (eye-the-door-and-leave-maybe!)
      (if (null? ls)
          (apply fso
                 (if (goal-achieved? ra)
                     (list "valid solution, steps: ~A, fuel usage: ~A~%"
                           step fuel)
                     (list "invalid solution~%")))
          (let* ((jets (car ls))
                 (L? (< 0 (logand 1 jets)))
                 (B? (< 0 (logand 2 jets)))
                 (R? (< 0 (logand 4 jets))))
            (update! ra L? B? R?)
            (spew! (person-picked-up? ra)
                   L? B? R?
                   (1+ step)
                   (ship-x  ship) (ship-y  ship)
                   (ship-vx ship) (ship-vy ship))
            (loop (cdr ls)
                  (1+ step)
                  (apply + fuel (map (lambda (x) (if x 1 0))
                                     (list L? B? R?)))))))))

(define (simulate! init solution)
  (let* ((zoom 2) (z10 (* zoom 10))
         (canvas #f) (fm #f)
         (hud-L #f) (hud-B #f) (hud-R #f)
         (shyph #f) (start-cell #f) (pntbm-cell #f))
    (run-solution!
     init solution
     (cons
      (lambda (ra)
        (let* ((dim (map (lambda (x)
                           (* z10 x))
                         (array-dimensions ra)))
               (h (car dim))
               (w (cadr dim))
               (w/3 (quotient w 3)))
          (set! hud-L (SDL:make-rect (* 2 w/3) h w/3 10))
          (set! hud-B (SDL:make-rect (* 1 w/3) h w/3 10))
          (set! hud-R (SDL:make-rect (* 0 w/3) h w/3 10))
          (set! canvas (simple-canvas #t w (+ 10 h) 32)))
        (set! fm (GFX:make-fps-manager 15))
        (let ((ship (ship ra)))
          (set! shyph (SDL:make-rect (* zoom (ship-x ship))
                                     (* zoom (ship-y ship))
                                     z10 z10)))
        (canvas #:clear!)
        (let ((cell (SDL:make-rect 0 0 z10 z10)))
          (define (copy)
            (SDL:make-rect (SDL:rect:x cell) (SDL:rect:y cell) z10 z10))
          (array-index-map!
           ra (lambda (y x)
                (SDL:rect:set-x! cell (* z10 x))
                (SDL:rect:set-y! cell (* z10 y))
                (let ((c (array-ref ra y x)))
                  (SDL:fill-rect
                   (canvas) cell
                   (case c
                     ((#\O) (set! start-cell (copy)) #xffffff)
                     ((#\+) (set! pntbm-cell (copy)) #x0000ff)
                     ((#\space) 0)
                     ((#\#) #xff0000)))
                  c))))
        (SDL:flip)
        (SDL:delay 1000))
      (lambda (yay! L? B? R? s x y vx vy)
        (SDL:fill-rect (canvas) shyph 0)
        (SDL:fill-rect (canvas) start-cell #xffffff)
        (and yay! (SDL:fill-rect (canvas) pntbm-cell 0))
        (SDL:rect:set-x! shyph (* zoom x))
        (SDL:rect:set-y! shyph (* zoom y))
        (SDL:fill-rect (canvas) shyph #x00ff00)
        (SDL:fill-rect (canvas) hud-L (if L? #xffff00 0))
        (SDL:fill-rect (canvas) hud-B (if B? #xffff00 0))
        (SDL:fill-rect (canvas) hud-R (if R? #xffff00 0))
        (SDL:flip)
        (GFX:fps-manager-delay! fm)))))
  (SDL:delay 3000))

(define (main/qop qop)
  (let* ((init (false-if-exception (car (qop '()))))
         (sol (and init (string-append init "." (or (qop 'solution) "BEST")))))
    (or (and init (file-exists? init))
        (decry "could not open mapfile"))
    (or (and sol (file-exists? sol))
        (decry "could not open solution file"))
    (ignore-all-event-types-except 'key-down)
    (simulate! init (read-line (open-input-file sol)))
    #t))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.5")
                   (help . commentary)))
  (and (null? (cdr args))
       (let ((me (car args)))
         (define (recurse mapfile solution)
           (zero?
            (system (fs "~S ~S~A" me
                        (datafile (in-vicinity "maps" (symbol->string
                                                       mapfile)))
                        (if solution
                            (fs " -s ~A" solution)
                            "")))))
         (exit (and (recurse 'simple 'drop-and-resettle)
                    (recurse 'simple 'slide-from-right)
                    (recurse 'simple #f)
                    (recurse 'well #f)))))
  (main/qop (qop<-args args '((solution (single-char #\s) (value #t))))))

;;; mars-rescue ends here
