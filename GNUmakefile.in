# GNUmakefile
#
# Copyright (C) 2010, 2011, 2012, 2013, 2021, 2022 Thien-Thi Nguyen
#
# This file is part of Guile-SDL Demos.
#
# Guile-SDL Demos is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Guile-SDL Demos is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

PACKAGE := @PACKAGE_NAME@
VERSION := @PACKAGE_VERSION@

PACKAGE-TARNAME := @PACKAGE_TARNAME@

SUBDIRS :=
SUBDIRS += deal
SUBDIRS += igm
SUBDIRS += mars-rescue
SUBDIRS += mpw
SUBDIRS += mq
SUBDIRS += robots
SUBDIRS += space-frisk
SUBDIRS += unpicasso
SUBDIRS += view-vcg

demos := $(foreach x,$(SUBDIRS),$(x)/$(x))

all: $(demos) bx/sdl-demos

debug-opts = $(and $(DEBUG),--debug)

%: %.scm
	@m=$$(sed '/^.define-module /!d;s///' $<) ;		\
	printf 'Creating %s as $@ ...\n' "$$m" ;		\
	printf "#!%s\n%s -L '%s' -e '%s' -s %s %s\n!#\n"	\
	  '/bin/sh'						\
	  'exec $${GUILE-guile} $(debug-opts)'			\
	  "$(shell pwd)/bx"					\
	  "$$m" '$$0 "$$@"'					\
	  '# -*-scheme-*-'					\
	  > $@
	@cat $< >> $@
	@chmod +x $@

bx/sdl-demos:
	cd bx && ln -sf .. sdl-demos

check:
	true $(foreach prog,$(SUBDIRS), && ./demo $(prog))

clean:
	rm -f $(demos) bx/sdl-demos demo-guile-sdl

###############################################
prefix := @prefix@
exec_prefix := @exec_prefix@
bindir := @bindir@
datarootdir := @datarootdir@
pkgdatadir := $(datarootdir)/$(PACKAGE-TARNAME)

useful-bx := guile-baux sdl-demos sdl-demos-common.scm

demo-guile-sdl: demo
	{ ./$< --list ; echo EOF ; echo ; } > $<.list
	{ ./$< --version | sed 's/^demo/&-guile-sdl/' ;	\
	  echo EOF ;					\
	  echo ;					\
	} > $<.version
	< $<					\
	sed -e 's/^# demo/&-guile-sdl/'		\
	    -e 's/^# .......demo/&-guile-sdl/'	\
	    -e '/0<configure.ac.*/{'		\
	    -e '  s//cat <<EOF/'		\
	    -e '  r demo.version'		\
	    -e '  p'				\
	    -e '  s/.*/ZONK/'			\
	    -e '}'				\
	    -e '/0<GNUmakefile.in.*/{'		\
	    -e '  s//cat <<EOF/'		\
	    -e '  r demo.list'			\
	    -e '  p'				\
	    -e '  s/.*/ZONK/'			\
	    -e '}'				\
	    -e '/ZONK/,/^$$/d'			\
	    -e '/^pkg/s|.$$|"$(pkgdatadir)"|'	\
	> $@
	chmod +x $@
	rm -f $<.list $<.version

install: demo-guile-sdl
	mkdir -p $(pkgdatadir)/.sup $(bindir)
	cp -p -f -r				\
	  $(addprefix bx/, $(useful-bx))	\
	  $(pkgdatadir)/.sup/
	ls='$(shell ./demo --list)' ;					\
	cp --parents -p -f -r $$ls $(pkgdatadir)/ ;			\
	for d in $$ls ; do						\
	  f=$(pkgdatadir)/$$d/$$d ;					\
	  cp -f $$f $$f.scm ;						\
	  sed "2s|'/.*/bx'|'$(pkgdatadir)/.sup'|" $$f.scm > $$f ;	\
	  rm -f $$f.scm ;						\
	done
	for f in $(addprefix $(pkgdatadir)/igm/, grok play) ; do	\
	  cp -f $$f $$f.tem ;						\
	  chmod +w $$f ;						\
	  sed "2s|'../bx'|'$(pkgdatadir)/.sup'|" $${f}.tem > $$f ;	\
	  rm -f $$f.tem ;						\
	done
	cp -p -f $< $(bindir)
	cp -p -f AUTHORS $(pkgdatadir)

uninstall:
	rm -rf $(pkgdatadir)
	rm -f $(bindir)/demo-guile-sdl

###################################
dd := $(PACKAGE-TARNAME)-$(VERSION)
extradist := configure bx/guile-baux.state bx/guile-baux/* igm/data/*.metrics
minusdist := .gitignore autogen.sh

dist:
	@test -d .git || { echo ERROR: No .git subdir. ; false ; }
	rm -rf $(dd) $(dd).tar.xz
	mkdir $(dd)
	cp -p --parents $(wildcard $(extradist)) \
	  $(shell git ls-files $(addprefix -x , $(wildcard $(minusdist)))) \
	  $(dd)
	tar cf $(dd).tar.xz --auto-compress $(dd)
	rm -rf $(dd)

# GNUmakefile ends here
