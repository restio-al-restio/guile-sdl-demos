;;; unpicasso

;; Copyright (C) 2007, 2011, 2013 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: unpicasso [OPTIONS...] [IMAGE...]
;;
;; Display some scrambled polygons and IMAGE (.gif, .png, .jpg)
;; files, and then unscramble them.  Options can include:
;;
;; -g, --geometry WxH  -- specify window size in pixels [1024x768]
;; -f, --fullscreen    -- toggle fullscreen mode [window]
;; -c, --count N       -- do scramble (and descramble) N times [42]
;;     --bpp N         -- use N bpp canvas [32]
;;     --random        -- use locate(1) to find a dir w/ .jpg files
;;     --loop          -- repeat indefinitely (good with --random)
;;
;; IMAGE may also name a directory, in which case it will be scanned
;; for image files, and the directory and the count of image files
;; it contains is displayed to stdout.
;;
;; Type ESC to quit at any time.

;;; Code:

(define-module (sdl-demos unpicasso)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile))
  #:use-module ((guile-baux common) #:select (fso check-hv qop<-args))
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((ice-9 popen) #:select (open-input-pipe
                                        close-pipe))
  #:use-module ((srfi srfi-4) #:select (list->s16vector))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-take-right))
  #:use-module ((sdl sdl) #:prefix SDL:)
  #:use-module ((sdl gfx) #:prefix GFX:)
  #:use-module ((sdl misc-utils) #:prefix MISC:)
  #:use-module ((sdl simple) #:select (simple-canvas)))

(define w 1024)
(define h 768)

(define (not-bored-yet?)
  (SDL:pump-events)
  (or (zero? (SDL:evqueue-peek 1 'key))
      (let ((ev (SDL:make-event)))
        (SDL:wait-event ev)
        (or (not (eq? 'key-down (SDL:event:type ev)))
            (not (eq? 'escape (SDL:event:key:keysym:sym ev)))
            (throw 'bored!)))))

(define (sit-for n)
  (let ((fps (GFX:make-fps-manager 25)))
    (do ((n n (max 0 (- n 40))))
        ((zero? n))
      (not-bored-yet?)
      (GFX:fps-manager-delay! fps))))

(define int<-  MISC:exact-truncate)

(define (/10-int n)
  (int<- (quotient n 10)))

(define (munge count show work image-filenames)

  (define (both proc)
    (proc work)
    (proc show))

  (define (poly!)
    (let* ((n (+ 3 (random 5)))
           (v (lambda (lim)
                (list->s16vector
                 (map (lambda (x)
                        (random lim))
                      (iota n)))))
           (xvec (v w))
           (yvec (v h))
           (color (+ #x42 (ash (random #xffffff) 8))))
      (both (lambda (surface)
              (GFX:draw-polygon surface xvec yvec color #t)
              (SDL:flip surface)))))

  (define (mostrare-immagine img)       ; adapted from tastiera-dura
    (let* ((iw  (SDL:surface:w img))
           (ih  (SDL:surface:h img))
           (ret (SDL:make-rect 0 0 iw ih)))
      (define (safe-random n)
        (random (max 1 (int<- n))))
      (define (rand-XY! W H)
        (SDL:rect:set-x! ret (safe-random (- w W)))
        (SDL:rect:set-y! ret (safe-random (- h H))))
      (cond
       ;; va bene
       ((and (< iw w)
             (< ih h))
        (rand-XY! iw ih)
        (SDL:blit-surface img #f work ret))
       ;; troppo grande
       (img
        (let* ((f (+ 0.1 (random 0.5)))
               (zfat (min (/ (* f w) iw)
                          (/ (* f h) ih))))
          (rand-XY! (* zfat iw) (* zfat ih))
          (SDL:blit-surface (GFX:zoom-surface img zfat) #f work ret))))
      ;; così esplicito perché come SDL gestisce
      ;; la memoria è opacco a guile
      (gc)))

  (do ((i 0 (1+ i)))
      ((= i 5))
    (poly!))
  (sit-for 750)

  (for-each (lambda (filename)
              (and (= 0 (random 3)) (poly!))
              (and=> (and (not-bored-yet?)
                          (SDL:load-image filename))
                     mostrare-immagine))
            (image-filenames))

  (let* ((acc '())
         (ramp (map 1+ (append (make-list 11 0)
                               (identity (iota 11))
                               (make-list 11 11)
                               (reverse! (iota 11))
                               (make-list 11 0))))
         (ramp-sum (apply + ramp)))
    (do ((i 0 (1+ i)))
        ((= i count))
      (let* ((x (random w))
             (y (random h))
             (bbw (random (- w x)))
             (bbh (random (- h y)))
             (bb (SDL:make-rect x y bbw bbh))
             (dx (/10-int (if (zero? bbw) bbw (- (random (* 2 bbw)) bbw))))
             (dy (/10-int (if (zero? bbh) bbh (- (random (* 2 bbh)) bbh)))))
        (define (box color)
          (GFX:draw-rectangle show x y (+ x bbw -1) (+ y bbh -1)
                              (+ #xff (ash color 8)))
          (SDL:update-rect show bb))
        (set! acc (cons (list (- dx) (- dy) bb #t) acc))
        (box #xffffff)
        (let-values
            (((ini! pan! fin!)
              (MISC:toroidal-panner/3p work dx dy bb)))
          (ini! ramp-sum)
          (while (and (not-bored-yet?)
                      (pan!)))
          (fin!))
        (box #x000000)))
    (sit-for 2000)

    (let-values (((init! fade! fini!)
                  (MISC:fader/3p 5 show #f show work)))
      (init!)
      (while (and (not-bored-yet?)
                  (fade!)))
      (fini!))
    (sit-for 2000)

    (for-each (lambda (back)
                (for-each (lambda (r)
                            (let-values
                                (((ini! pan! fin!)
                                  (apply MISC:toroidal-panner/3p show back)))
                              (ini! r)
                              (while (and (not-bored-yet?)
                                          (pan!)))
                              (fin!)))
                          ramp)
                (sit-for 200))
              acc)
    (sit-for 5000)))

(define (loopy more? count show work image-filenames)
  (let loop ()
    (munge count show work image-filenames)
    (and more?
         (let ((dim (MISC:copy-surface show)))
           (SDL:fill-rect dim #f 0)
           (SDL:surface-alpha! dim 30)
           (do ((i 0 (1+ i)))
               ((= 9 i))
             (SDL:blit-surface dim #f show)
             (SDL:flip show))
           (SDL:blit-surface show #f work)
           (SDL:delay 420)
           (loop)))))

(define (selected-directory-filenames sel dir)
  (let ((d (opendir dir)))
    (let loop ((acc '()))
      (let ((filename (readdir d)))
        (cond ((eof-object? filename)
               (closedir d)
               acc)
              ((sel filename)
               (loop (cons (in-vicinity dir filename) acc)))
              (else
               (loop acc)))))))

(define (image-file?-proc extensions)
  (define (filename-extension filename)
    (and=> (string-rindex filename #\.)
           (lambda (pos)
             (let ((len (- (string-length filename) pos 1)))
               (and (positive? len)
                    (string->symbol (string-take-right filename len)))))))
  (lambda (filename)
    (memq (filename-extension filename)
          extensions)))

(define (directory-images-proc extensions)
  (let ((sel (image-file?-proc extensions)))
    (lambda (dir)
      (let ((ls (selected-directory-filenames sel dir)))
        (fso "~A: ~S~%" dir (length ls))
        ls))))

(define (random-vector-ref v)
  (vector-ref v (random (vector-length v))))

(define (from-locate directory-images)
  (let* ((p (open-input-pipe "locate .jpg"))
         (dirs (let loop ((acc '()))
                 (let ((filename (read-line p)))
                   (if (eof-object? filename)
                       (begin
                         (close-pipe p)
                         acc)
                       (let ((dir (dirname filename)))
                         (loop (if (member dir acc)
                                   acc
                                   (cons dir acc)))))))))
    (if (null? dirs)
        dirs
        (directory-images (random-vector-ref (list->vector dirs))))))

(define (main/qop qop)
  (set! *random-state* (seed->random-state (let ((now (gettimeofday)))
                                             (* (car now) (cdr now)))))
  (and=> (qop 'geometry MISC:rectangle<-geometry-string)
         (lambda (rect)
           (set! w (SDL:rect:w rect))
           (set! h (SDL:rect:h rect))))
  (let* ((C (simple-canvas #t w h (or (qop 'bpp string->number) 32)))
         (work (MISC:copy-surface (C))))
    (dynamic-wind
        (lambda ()
          (or (SDL:show-cursor)
              (SDL:show-cursor))
          (and (qop 'fullscreen)
               (SDL:toggle-full-screen)))
        (lambda ()
          (catch 'bored!
                 (lambda ()
                   (define specified-image-filenames
                     (let ((dir-images (directory-images-proc
                                        '(gif png jpg ppm))))
                       (lambda ()
                         (apply append
                                (if (qop 'random)
                                    (from-locate dir-images)
                                    (if (null? (qop '()))
                                        ;; what we share, what we bear;
                                        ;; should we care, how we fare?
                                        (list (datafile "earth.jpg"))
                                        '()))
                                (map (lambda (x)
                                       (if (file-is-directory? x)
                                           (dir-images x)
                                           (list x)))
                                     (qop '()))))))
                   (loopy (qop 'loop)
                          (or (qop 'count string->number) 42)
                          (C) work specified-image-filenames))
                 (lambda args args)))
        (lambda ()
          (and (qop 'fullscreen)
               (SDL:toggle-full-screen))
          (SDL:quit)))
    #t))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.7")
                   (help . commentary)))
  (main/qop (qop<-args args
                       '((bpp (value #t))
                         (geometry (single-char #\g) (value #t))
                         (fullscreen (single-char #\f))
                         (loop)
                         (random)
                         (count (single-char #\c) (value #t))))))

;;; unpicasso ends here
