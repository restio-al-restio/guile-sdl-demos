;;; robots --- a clone of GNU Robots

;; Copyright (C) 2012, 2013, 2021 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: robots [options] [FILE]
;;
;; Game/diversion where you construct a program for a little robot
;; then set him loose and watch him explore a world on his own.
;;
;; Options (defaults in square braces) are:
;;  -f, --map-file FILE   -- Load map file FILE [small.map]
;;  -s, --shields N       -- Set initial shields to N [100]
;;  -e, --energy N        -- Set initial energy to N [1000]
;;
;; FILE is a scheme script that defines the robot behavior;
;; default is greedy.scm (see subdir ‘behaviors’ for more).
;;
;; Type ESC to interrupt, again to quit.

;;; Code:

(define-module (sdl-demos robots)
  #:export (main
            robot-turn
            robot-move
            robot-smell
            robot-feel
            robot-look
            robot-grab
            robot-zap
            robot-stop
            robot-get-shields
            robot-get-energy
            robot-get-score)

  #:use-module ((sdl-demos-common)
                #:select (datafile decry))

  #:use-module ((guile-baux common)
                #:select (fs check-hv qop<-args))

  #:use-module ((srfi srfi-1)
                #:select (circular-list))

  #:use-module ((srfi srfi-11)
                #:select (let-values))

  #:use-module ((ice-9 rdelim)
                #:select (read-line))

  #:use-module ((sdl sdl)
                #:prefix SDL:
                #:select (enable-key-repeat
                          load-image
                          flip
                          delay
                          make-rect
                          fill-rect
                          update-rect
                          rect:x rect:y rect:w rect:h
                          rect:set-x! rect:set-y!
                          make-surface
                          surface:w surface:h
                          blit-surface
                          make-event
                          poll-event
                          event:type
                          event:key:keysym:sym))

  #:use-module ((sdl gfx)
                #:prefix GFX:
                #:select (draw-string))

  #:use-module ((sdl misc-utils)
                #:select (rect<-surface
                          copy-surface
                          exact-truncate
                          poll-with-push-on-timeout-proc))

  #:use-module ((sdl simple)
                #:select (simple-canvas)))

(define USER-INPUT (SDL:make-event))

(define (ESC-pressed? ev)
  (and (eq? 'key-down (SDL:event:type ev))
       (eq? 'escape (SDL:event:key:keysym:sym ev))))

(define SCORE 0)

(define +DIRECTIONS+ #(north east south west))

(define X 1)
(define Y 1)
(define DIRECTION 1)                    ; east
(define ENERGY 1000)
(define SHIELDS 100)
(define UNITS-WALKED 0)
(define SHOTS-FIRED 0)

(define (direction)
  (vector-ref +DIRECTIONS+ DIRECTION))

(define STATUS-TEXT (cons "" ""))

(define (update-status-text!)
  (set-car! STATUS-TEXT (fs "(score ~A) (energy ~A) (shields ~A)"
                            SCORE ENERGY SHIELDS))
  (set-cdr! STATUS-TEXT (fs "(units-walked ~A) (shots-fired ~A)"
                            UNITS-WALKED SHOTS-FIRED)))

(define (add-energy! n)
  (set! ENERGY (+ ENERGY n))
  (update-status-text!))

(define (die! reason)
  (throw 'death reason))

(define (use-energy! n)
  (add-energy! (- n))
  (or (positive? ENERGY)
      (die! 'no-more-energy)))

(define (ablate! n)
  (set! SHIELDS (- SHIELDS n))
  (update-status-text!)
  (or (positive? SHIELDS)
      (die! 'no-more-shields))
  #f)

(define (inc-units-walked!)
  (set! UNITS-WALKED (1+ UNITS-WALKED))
  (update-status-text!))

(define WORLD #f)

(define (at x y)
  (array-ref WORLD y x))

(define (at! x y thing)
  (array-set! WORLD thing y x))

(define (clear! x y)
  (at! x y 'space))

(define (robot!)
  (at! X Y (symbol-append 'robot_ (direction))))

(define IMAGES #f)

(define (image name)
  (assq-ref IMAGES name))

(define DIMS #f)
;; This is solely to be able to use ‘array-index-map!’ in ‘update-world!’.
;; There must be a better way.
(define LAME-ARRAY-API-KLUDGE #f)
(define CANVAS #f)
(define BACKGROUND #f)
(define BLURB #f)

(define (set-up-world! filename)

  (define (decode c)
    (case c
      ((#\+) 'food)
      ((#\$) 'prize)
      ((#\#) 'wall)
      ((#\@) 'baddie)
      (else 'space)))

  (define (image name)
    (SDL:load-image (datafile (fs "images/~A.xpm" name))))

  (let ((p (open-input-file filename)))
    (let loop ((acc '()))
      (let ((line (read-line p)))
        (cond ((string? line)
               (loop (cons `(#f
                             ,@(map decode (string->list line))
                             #f)
                           acc)))
              (else
               (close-port p)
               (set! WORLD (list->array
                            '(-1 -1)
                            (let ((empty (make-list (length (car acc)) #f)))
                              `(,empty
                                ,@(reverse! acc)
                                ,empty))))
               (set! IMAGES (map (lambda (name)
                                   (cons name (image name)))
                                 '(baddie
                                   food
                                   prize
                                   robot
                                   robot_east
                                   robot_north
                                   robot_south
                                   robot_west
                                   space
                                   statusbar
                                   wall)))

               (set! DIMS (array-dimensions WORLD))
               (set! LAME-ARRAY-API-KLUDGE (apply make-array #f DIMS))
               (let* ((statusbar (image 'statusbar))
                      (s-h (SDL:surface:h statusbar))
                      (s-w (SDL:surface:w statusbar))
                      (h (+ 2 (cadar DIMS)))
                      (w (+ 2 (cadadr DIMS))))
                 (set! CANVAS (simple-canvas #t (* w 16)
                                             (+ (* h 16) s-h)
                                             32))
                 (set! BACKGROUND (copy-surface (CANVAS)))
                 (let ((w (SDL:surface:w BACKGROUND))
                       (h (SDL:surface:h BACKGROUND))
                       (rect (rect<-surface statusbar)))
                   (set! BLURB (- h s-h))
                   (SDL:rect:set-y! rect BLURB)
                   (do ((x 0 (+ x s-w)))
                       ((< w x))
                     (SDL:rect:set-x! rect x)
                     (SDL:blit-surface statusbar #f BACKGROUND rect))
                   ))))))))

(define (draw-status! surface pair)
  (GFX:draw-string surface 4 (+  6 BLURB) (car pair) #xffffffff)
  (GFX:draw-string surface 4 (+ 18 BLURB) (cdr pair) #xffffffff))

(define (ofs n)
  ;; i.e., (+ 16 (* 16 n))
  (ash (1+ n) 4))

(define update-world!
  (let ((tile (SDL:make-rect 0 0 16 16))
        (surface #f))

    (define (all!)
      (SDL:blit-surface BACKGROUND #f surface)
      (array-index-map!
       LAME-ARRAY-API-KLUDGE            ; ugh
       (lambda (y x)
         (and=> (at x y)
                (lambda (name)
                  (SDL:rect:set-x! tile (ofs x))
                  (SDL:rect:set-y! tile (ofs y))
                  (SDL:blit-surface (image name) #f surface tile)))))
      (draw-status! surface STATUS-TEXT)
      (SDL:flip))

    (define (blink rect)
      (define (paint! color)
        (SDL:fill-rect surface rect color)
        (SDL:update-rect surface rect)
        (SDL:delay 10))
      (do ((i 0 (1+ i)))
          ((= 5 i))
        (paint! #xff00ff)
        (paint! #x000000)))

    (lambda in-particular
      (or surface (set! surface (CANVAS))) ; ugh
      (and (SDL:poll-event USER-INPUT)
           (ESC-pressed? USER-INPUT)
           (die! 'impatient-user))
      (if (null? in-particular)
          (all!)
          (let ((kind (car in-particular))
                (args (cdr in-particular)))
            (case kind
              ((sniff)
               (blink (SDL:make-rect (ofs (1- X)) (ofs (1- Y)) 48 48))
               (all!))
              ((feel grab zap)
               (SDL:rect:set-x! tile (ofs (car args)))
               (SDL:rect:set-y! tile (ofs (cadr args)))
               (blink tile)
               (all!))
              (else (decry "bad in-particular: ~S" in-particular))))))))

(define (robot-turn n)
  (let loop ((n n))
    (or (zero? n)
        (let ((sign (/ n (abs n))))
          (set! DIRECTION (modulo (+ DIRECTION sign) 4))
          (robot!)
          (use-energy! 2)
          (update-world!)
          (loop (- n sign))))))

(define-macro (normalize! thing)
  `(set! ,thing (if (string? ,thing)
                    (string->symbol ,thing)
                    ,thing)))

(define (deltas sign)
  (case (direction)
    ((north) (values    0     (- sign)))
    ((east)  (values    sign     0))
    ((south) (values    0        sign))
    ((west)  (values (- sign)    0))))

(define (robot-move n)
  (or (zero? n)
      (let ((sign (/ n (abs n))))
        (let-values (((dx dy) (deltas sign)))
          (let loop ((n n))
            (or (zero? n)
                (let ((x (+ X dx))
                      (y (+ Y dy)))
                  (use-energy! 2)
                  (case (at x y)
                    ((space)
                     ;; move the robot there
                     (clear! X Y)
                     (set! X x)
                     (set! Y y)
                     (robot!)
                     (update-world!)
                     (inc-units-walked!)
                     (loop (+ n sign)))
                    ((baddie)
                     ;; big damage
                     (ablate! 10))
                    ((wall)
                     ;; lesser damage
                     (ablate! 2))
                    (else
                     ;; even less damage
                     (ablate! 1))))))))))

(define (robot-smell thing)
  (normalize! thing)
  (use-energy! 1)
  (update-world! 'sniff)
  (call-with-current-continuation
   (lambda (ret)
     (do ((x (- X 1) (1+ x)))
         ((= (+ X 2) x))
       (do ((y (- Y 1) (1+ y)))
           ((= (+ Y 2) y))
         (and (eq? thing (at x y))
              (ret #t))))
     #f)))

(define (in-front)
  (let-values (((dx dy) (deltas 1)))
    (values (+ X dx)
            (+ Y dy))))

(define (robot-feel thing)
  (normalize! thing)
  (use-energy! 1)
  (let-values (((x y) (in-front)))
    (update-world! 'feel x y)
    (let ((there (at x y)))
      ;; touching a baddie is hurtful
      (and (eq? 'baddie there)
           (ablate! 0))
      (eq? thing there))))

(define (robot-look thing)
  (normalize! thing)
  (use-energy! 1)
  (let-values (((dx dy) (deltas 1)))
    (let loop ((x (+ X dx))
               (y (+ Y dy)))
      (let ((there (at x y)))
        (or (eq? thing there)
            (and (eq? 'space there)
                 (loop (+ x dx)
                       (+ y dy))))))))

(define (robot-grab)
  (use-energy! 5)
  (let-values (((x y) (in-front)))
    (let ((there (at x y)))
      (update-world! 'grab x y)
      (case there
        ((food prize)
         (cond ((eq? 'food there)
                (add-energy! 15))
               (else
                (set! SCORE (1+ SCORE))
                (update-status-text!)))
         (clear! x y)
         (update-world!)
         #t)
        ((baddie)
         (ablate! 10))
        (else
         #f)))))

(define (robot-zap)
  (use-energy! 10)
  (let-values (((x y) (in-front)))
    (set! SHOTS-FIRED (1+ SHOTS-FIRED))
    (update-status-text!)
    (update-world! 'zap x y)
    (and (not (memq (at x y) '(space wall robot)))
         (begin
           (clear! x y)
           (update-world!)
           #t))))

(define (robot-stop)
  (die! 'boredom))

(define (robot-get-shields) SHIELDS)
(define (robot-get-energy)  ENERGY)
(define (robot-get-score)   SCORE)

(define (execute-script filename)
  (let* ((kludge "robots-TMP")
         (p (open-output-file kludge)))
    (write '(use-modules (sdl-demos robots)) p)
    (newline p)
    (close-port p)
    (and (zero? (system (fs "cat ~A >> ~A" filename kludge)))
         (catch 'death
                (lambda ()
                  (primitive-load kludge)
                  "OK: done!")
                (lambda (key . args)
                  (delete-file kludge)
                  (case (car args)
                    ((boredom) "OK: quit!")
                    (else (fs "BUMMER: death! ~S" args))))))))

(define (wait-for-ESC status)
  (let ((ev (SDL:make-event))
        (surface (CANVAS))
        (check (poll-with-push-on-timeout-proc 1800 100))
        (zonkable (rect<-surface BACKGROUND 0 BLURB)))

    (define (spew! pair)
      (SDL:blit-surface BACKGROUND zonkable surface #f)
      (draw-status! surface pair)
      (SDL:flip))

    (let loop ((cycle (circular-list
                       (cons status
                             "type ESC to quit")
                       (cons "check out the real GNU Robots"
                             "https://www.gnu.org/software/gnurobots/")
                       STATUS-TEXT)))
      (spew! (car cycle))
      (or (and (check ev)
               (ESC-pressed? ev))
          (loop (cdr cycle))))))

(define (main/qop qop)
  (define (integer s)
    (exact-truncate (string->number s)))
  (qop 'shields (lambda (s) (set! SHIELDS (integer s))))
  (qop 'energy (lambda (s) (set! ENERGY (integer s))))
  (set-up-world!
   (or (qop 'map-file (lambda (filename)
                        (if (file-exists? filename)
                            filename
                            (datafile (in-vicinity "maps" filename)))))
       (datafile "maps/small.map")))
  (robot!)
  (update-world!)
  (SDL:enable-key-repeat 0 0) ;;; TODO: ‘(sdl misc-utils) disable-key-repeat’
  (wait-for-ESC
   (execute-script
    (if (null? (qop '()))
        (datafile "behaviors/greedy.scm")
        (let ((filename (car (qop '()))))
          (if (file-exists? filename)
              filename
              (datafile (in-vicinity "behaviors" filename))))))))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.2")
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((map-file (value #t) (single-char #\f))
           (shields (value #t) (single-char #\s))
           (energy (value #t) (single-char #\e))))))

;;; robots ends here
