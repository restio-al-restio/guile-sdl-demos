;;; space-frisk --- visualize frisk results

;; Copyright (C) 2003, 2004, 2005, 2006, 2007,
;;   2011, 2012, 2013, 2021 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: space-frisk [OPTIONS...] [FILE ...]
;;
;; Visualize results of frisking FILE ... in a window.  Internal modules
;; form the inner "sphere", external the outer.  Key bindings:
;;
;;  Arrow keys in conjunction w/ right-shift
;;         -- change the rotational velocity for different axes
;;  SPACE  -- pause
;;  RET    -- kill all velocities
;;  n      -- choose a new random arrangement
;;  a      -- toggle "auto-tweak mode"
;;  l      -- toggle module labels
;;  e      -- toggle edge visibility
;;  v      -- toggle vertice image visibility
;;  r      -- cycle through four "refresh modes"
;;  o      -- select one module; display up and dn modules, and info
;;  p      -- select all modules (default)
;;  ESC    -- quit
;;
;; Command line options [default values] are:
;;  -g, --geometry WxH  -- specify W by H pixel display [999x451]
;;      --seed N        -- use integer N to seed the PRNG [current time]
;;      --font-size N   -- use font sized N (max 64) for labels [24]
;;      --site          -- include scheme files from the site dir(s),
;;                         normally /usr/local/lib/guile/site
;;                              and /usr/local/share/guile/site
;;
;; For more info, see module (scripts frisk), part of Guile.

;;; Code:

(define-module (space-frisk)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile decry))
  #:use-module ((guile-baux common) #:select (fs fso check-hv qop<-args))
  #:use-module ((ice-9 ftw) #:select (nftw))
  #:use-module ((srfi srfi-1) #:select (filter))
  #:use-module ((srfi srfi-13) #:select (string-prefix?
                                         string-suffix?))
  #:use-module ((guile-baux frisker) #:select (frisker
                                               mod-int? mod-up-ls mod-down-ls
                                               edge-up
                                               edge-down))
  #:use-module ((sdl sdl) #:prefix ///-)
  #:use-module ((sdl ttf) #:prefix /T/-)
  #:use-module ((sdl gfx) #:prefix /G/-)
  #:use-module ((sdl misc-utils) #:prefix /M/-)
  #:use-module ((sdl simple) #:select ((simple-canvas . /S/-canvas)
                                       (simple-stylus . /S/-stylus)
                                       (simple-vpacked-image . /S/-vpkimg))))

(define (fln s . args)
  (apply fso s args)
  (newline))

(define visible:: (make-object-property))

(define int<- /M/-exact-truncate)

(define (int-/ top bot)
  (int<- (/ top bot)))

(define (display-graphically graph screen-w screen-h font-size)
  (define (ehalf n) (int-/ n 2))
  (graph #:display-summary!)
  (let ((verts (graph #:vertices))
        (relevant-verts #f)
        (edges (graph #:edges))
        (relevant-edges #f)
        (Z #f)
        (tela (/S/-canvas #t screen-w screen-h 8))
        (max-radius (- (ehalf (min screen-w screen-h)) 8))
        (balls (let ((blu (/S/-vpkimg (datafile "blueball.png")))
                     (red (/S/-vpkimg (datafile "redball.png"))))
                 (lambda (key)
                   (case key
                     ((#:blu) blu)
                     ((#:red) red)
                     ((#:num) (vector-length (blu #:rects)))))))
        (xy-vel 0.0) (xz-vel 0.0) (yz-vel 0.0)
        (stylus (/S/-stylus #t (datafile "FreeSerif.ttf") (min 64 font-size)
                            255 255 255))
        (refresh-mode 0)
        (vertices? #t)
        (edge-color #f)
        (labels? #f)
        (realize: (make-object-property))
        (z: (make-object-property))
        (special?: (make-object-property))
        (pos: (make-object-property))
        (rtext: (make-object-property))
        (rinfo: (make-object-property)))

    (define (update-relevancy!)
      (set! relevant-verts
            (list->vector
             (let loop ((ls verts) (acc '()))
               (if (null? ls)
                   acc
                   (loop (cdr ls) (if (visible:: (car ls))
                                      (cons (car ls) acc)
                                      acc))))))
      (set! relevant-edges
            (list->vector
             (if edge-color
                 (let loop ((ls edges) (acc '()))
                   (if (null? ls)
                       acc
                       (loop (cdr ls) (if (visible:: (car ls))
                                          (cons (car ls) acc)
                                          acc))))
                 '())))
      (let* ((v-valid (if vertices? (vector-length relevant-verts) 0))
             (e-valid (vector-length relevant-edges))
             (valid (+ v-valid e-valid)))
        (set! Z (make-vector valid))
        (array-index-map!
         Z (lambda (i)
             (let* ((v? (< i v-valid))
                    (obj (vector-ref (if v? relevant-verts relevant-edges)
                                     (if v? i (- i v-valid)))))
               (cons* (z: obj) (realize: obj) obj))))))

    (define draw-vert!
      (let ((ofs (list->vector (map ehalf (iota (balls #:num)))))
            (dst (///-make-rect 0 0 0 0)))
        (lambda (x y d blu/red)
          (let ((offset (vector-ref ofs d)))
            (///-rect:set-x! dst (- x offset))
            (///-rect:set-y! dst (- y offset))
            (///-rect:set-w! dst d)
            (///-rect:set-h! dst d))
          (blu/red #:blit! d dst))))

    (define (draw-edge! ux uy mx my dx dy)
      (/G/-draw-line (tela) ux uy mx my #xffffffff)
      (/G/-draw-line (tela) dx dy mx my edge-color))

    (define compute-rinfo!
      (let* ((blu (balls #:blu))
             (red (balls #:red))
             (max-depth-level (balls #:num))
             (center-x (/ screen-w 2))
             (center-y (/ screen-h 2))
             (chunk (/ (* 2 max-radius) (- max-depth-level 2)))
             (shift (ehalf max-depth-level)))
        (define (screen-x pos)
          (int<- (+ center-x (vector-ref pos 0))))
        (define (screen-y pos)
          (int<- (+ center-y (vector-ref pos 1))))
        (define (z->index z)
          (+ (int-/ z chunk) shift))
        ;; rv
        (lambda (vert)
          (let* ((pos (pos: vert))
                 (z (vector-ref pos 2)))
            (set! (z: vert) z)
            (set! (rinfo: vert) (list (screen-x pos) (screen-y pos)
                                      (z->index z)
                                      (if (special?: vert) red blu)))))))

    (define (compute-edge-rinfo!)
      (define (one! edge)
        (let* ((unode (edge-up edge))
               (dnode (edge-down edge))
               (up (rinfo: unode))
               (dn (rinfo: dnode))
               (ux (car up))
               (uy (cadr up))
               (uz (vector-ref (pos: unode) 2))
               (dx (car dn))
               (dy (cadr dn))
               (dz (vector-ref (pos: dnode) 2))
               (mx (ehalf (+ ux dx)))
               (my (ehalf (+ uy dy))))
          (set! (z: edge) (/ (+ uz dz) 2))
          (set! (rinfo: edge) (list ux uy
                                    mx my
                                    dx dy))))
      (and edge-color (array-for-each one! relevant-edges)))

    (define (move!)
      (define (compute-angle dx dy hyp)
        (if (< dx 0)
            (* (acos (/ dx hyp))
               (if (< dy 0)
                   -1
                   1))
            (asin (/ dy hyp))))
      (for-each (lambda (vert)
                  (let* ((pos (pos: vert))
                         (x (vector-ref pos 0))
                         (y (vector-ref pos 1))
                         (z (vector-ref pos 2))
                         (r (sqrt (+ (* x x) (* y y))))
                         (theta (compute-angle x y r))
                         (new-theta (+ theta xy-vel)))
                    ;; suboptimal
                    (set! x (* r (cos new-theta)))
                    (vector-set! pos 0 x)
                    (set! y (* r (sin new-theta)))
                    (set! r (sqrt (+ (* z z) (* y y))))
                    (set! theta (compute-angle z y r))
                    (set! new-theta (+ theta yz-vel))
                    (set! z (* r (cos new-theta)))
                    (vector-set! pos 2 z)
                    (vector-set! pos 1 (* r (sin new-theta)))
                    (set! r (sqrt (+ (* z z) (* x x))))
                    (set! theta (compute-angle z x r))
                    (set! new-theta (+ theta xz-vel))
                    (set! z (* r (cos new-theta)))
                    (set! x (* r (sin new-theta)))
                    (vector-set! pos 0 x)
                    (vector-set! pos 2 z))
                  (compute-rinfo! vert))
                verts)
      (compute-edge-rinfo!))

    (define random-position!
      (let ((inner (int-/ max-radius 3))
            (outer max-radius))
        (define (image! vert)
          (let* ((in? (graph #:special? vert))
                 (pos (vector 0 0 0))
                 (radius (if in? inner outer)))
            (random:hollow-sphere! pos)
            (do ((i 0 (1+ i)))
                ((= i 3))
              (vector-set! pos i (* radius (vector-ref pos i))))
            (set! (pos: vert) pos)
            (set! (special?: vert) in?)
            (compute-rinfo! vert)
            '(fln "~A ~A ~S"
                  (if in? 'i 'x) vert
                  (pos: vert))))
        (define (label! vert)
          (let* ((text (fs "~A" vert))
                 (rtext (stylus #:render text
                                (let ((bright (lambda ()
                                                (+ #x40 (random #xC0)))))
                                  (///-make-color (bright)
                                                  (bright)
                                                  (bright)))
                                #t))
                 (w (///-surface:w rtext)))
            (set! (rtext: vert)
                  (list rtext (///-make-rect (if (= 0 (random 2))
                                                 0
                                                 (- screen-w w))
                                             0
                                             w
                                             (///-surface:h rtext))))))
        ;; rv
        (lambda ()
          (for-each (lambda (vert)
                      (image! vert)
                      (label! vert))
                    verts)
          (compute-edge-rinfo!))))

    (define (refresh!)
      (define (Z-do proc) (array-for-each proc Z))
      (and (or (= 0 refresh-mode)
               (= 1 refresh-mode)
               (= 0 (random 1000)))
           (tela #:clear!))
      (Z-do (lambda (ent)
              (set-car! ent (z: (cddr ent)))))
      (sort! Z (lambda (a b)
                 (< (car a) (car b))))
      (Z-do (lambda (ent)
              (set! ent (cdr ent))
              (apply (car ent) (rinfo: (cdr ent)))))
      (and labels?
           (array-for-each (lambda (vert)
                             (apply (lambda (rtext rect)
                                      (///-rect:set-y! rect (cadr (rinfo: vert)))
                                      (///-blit-surface rtext #f (tela) rect))
                                    (rtext: vert)))
            relevant-verts))
      (and (or (= 0 refresh-mode)
               (= 2 refresh-mode)
               (= 0 (random 100)))
           (///-flip)))

    (define (update-relevancy+edge-rinfo!)
      (update-relevancy!)
      (compute-edge-rinfo!))

    ;; do it!
    (for-each (lambda (vert) (set! (realize: vert) draw-vert!)) verts)
    (for-each (lambda (edge) (set! (realize: edge) draw-edge!)) edges)
    (random-position!)
    (update-relevancy!)
    (/M/-ignore-all-event-types-except 'key-down)
    (///-enable-key-repeat 10 10)
    (let ((pause #f)
          (rate (/G/-make-fps-manager 20))
          (auto #f) (xy-mul #f) (xz-mul #f) (yz-mul #f)
          (tick 0))
      (define (rshift? ev)
        (equal? (///-event:key:keysym:mod ev) '(R-shift)))
      (let loop ((ev (///-make-event)))
        (/G/-fps-manager-delay! rate)
        (refresh!)
        (cond ((and (not pause) auto (= 0 (modulo tick 10)))
               (let ((tic (/ tick 10)))
                 (set! xy-vel (* 0.1 (sin (* tic xy-mul))))
                 (set! xz-vel (* 0.1 (sin (* tic xz-mul))))
                 (set! yz-vel (* 0.1 (sin (* tic yz-mul)))))))
        (or pause (move!))
        (set! tick (1+ tick))
        (and (///-poll-event ev)        ; set ‘ev’ by side effect
             (case (///-event:key:keysym:sym ev)
               ((escape)
                (set! ev #f))
               ((D-0 enter return)
                (set! auto #f)
                (set! xy-vel 0.0)
                (set! xz-vel 0.0)
                (set! yz-vel 0.0))
               ((up)
                (and (< -0.2 yz-vel)
                     (set! yz-vel (- yz-vel 0.01))))
               ((down)
                (and (> 0.2 yz-vel)
                     (set! yz-vel (+ yz-vel 0.01))))
               ((left)
                (if (rshift? ev)
                    (and (< -0.2 xy-vel)
                         (set! xy-vel (- xy-vel 0.01)))
                    (and (< -0.2 xz-vel)
                         (set! xz-vel (- xz-vel 0.01)))))
               ((right)
                (if (rshift? ev)
                    (and (> 0.2 xy-vel)
                         (set! xy-vel (+ xy-vel 0.01)))
                    (and (> 0.2 xz-vel)
                         (set! xz-vel (+ xz-vel 0.01)))))
               ((n)
                (random-position!)
                (gc))
               ((a)
                (cond (auto (set! auto #f))
                      (else
                       (set! xy-mul (random 0.5))
                       (set! xz-mul (random 0.5))
                       (set! yz-mul (random 0.5))
                       (set! tick 0)
                       (set! auto #t))))
               ((o)                     ; lame keybinding
                (graph #:select #:next)
                (update-relevancy+edge-rinfo!))
               ((p)                     ; lame keybinding
                (graph #:select #:all)
                (update-relevancy+edge-rinfo!))
               ((l)
                (set! labels? (not labels?)))
               ((e)
                (set! edge-color (if edge-color
                                     #f
                                     (+ #xff (ash (random #xffffff) 8))))
                (update-relevancy+edge-rinfo!))
               ((v)
                (set! vertices? (not vertices?))
                (update-relevancy!))
               ((r)
                (set! refresh-mode
                      (modulo (1+ refresh-mode) 4)))
               ((space)
                (set! pause (not pause)))))
        (and ev (loop ev)))))
  (///-quit))

(define (make-graph/frisking files)
  (let ((report ((frisker) files))
        (module-ring #f)
        (selection #f))
    (define (show! sel)
      (case sel
        ((#:all)
         (for-each (lambda (m) (set! (visible:: m) #t)) (report 'modules))
         (for-each (lambda (e) (set! (visible:: e) #t)) (report 'edges))
         (set! selection #f))
        ((#:next-module)
         (cond (module-ring)
               (else (set! module-ring (list-copy (report 'modules)))
                     (set-cdr! (last-pair module-ring) module-ring)))
         (for-each (lambda (m) (set! (visible:: m) #f)) (report 'modules))
         (for-each (lambda (e) (set! (visible:: e) #f)) (report 'edges))
         (and selection (set! module-ring (cdr module-ring)))
         (set! selection (car module-ring))
         (set! (visible:: selection) #t)
         (let* ((ue (mod-up-ls selection))
                (de (mod-down-ls selection))
                (up (map edge-up ue))
                (dn (map edge-down de)))
           (fso "~A -- ~A up ~A dn~%" selection (length up) (length dn))
           (for-each (lambda (x) (set! (visible:: x) #t))
                     (append ue de up dn))))))
    ;; initially everything is visible
    (show! #:all)
    ;; rv
    (lambda (command . args)
      (case command
        ((#:special?) (mod-int? (car args)))
        ((#:vertices) (report 'modules))
        ((#:vertices-are) "modules")
        ((#:edges) (report 'edges))
        ((#:display-summary!) (fln "~A modules, ~A internal, ~A external"
                                   (length (report 'modules))
                                   (length (report 'internal))
                                   (length (report 'external))))
        ((#:select) (show! (case (car args)
                             ((#:next) #:next-module)
                             (else (car args)))))
        (else (decry "bad command: ~S" command))))))

(define (site? dir)
  (string-suffix? "/site" dir))

(define (scheme-under dir)

  (define (from-module-catalog)

    (define (source?/filename catalog-entry)
      (let ((impl (cdr catalog-entry)))
        (and (string? impl)
             (if (string-prefix? "/" impl)
                 impl
                 (in-vicinity dir impl)))))

    (let ((cat (in-vicinity dir ".module-catalog")))
      (and (file-exists? cat)
           (delq! #f (map source?/filename
                          (read (open-input-file cat)))))))

  (define (from-file-system-walk)
    (let ((acc '()))
      (define (collect filename statinfo flag base level)
        (and (eq? 'regular flag)
             (set! acc (cons filename acc)))
        #t)
      (nftw dir collect 'depth)
      acc))

  ;; do it!
  (or (from-module-catalog)
      (from-file-system-walk)))

(define (site-scheme)
  (map scheme-under (filter site? %load-path)))

(define (main args)
  (define (main/qop qop)
    (let ((w/h (or (qop 'geometry
                        (lambda (s)
                          (string-set! s (string-index s #\x) #\space)
                          (with-input-from-string s
                            (lambda () (cons (read) (read))))))
                   '(999 . 451)))       ; bradbury saw it coming
          (graph (make-graph/frisking
                  (apply append (qop '())
                         (if (qop 'site)
                             (site-scheme)
                             (if (null? (qop '()))
                                 `((,(car (command-line))))
                                 '()))))))
      (set! *random-state* (seed->random-state
                            (or (qop 'seed string->number)
                                (let ((now (gettimeofday)))
                                  (* (car now) (cdr now))))))
      (if (null? (graph #:vertices))
          (fln "no ~A found" (graph #:vertices-are))
          (display-graphically graph (car w/h) (cdr w/h)
                               (or (qop 'font-size string->number)
                                   24)))))
  ;; do it!
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.38")
                   (help . commentary)))
  (main/qop (qop<-args args '((geometry (single-char #\g) (value #t))
                              (seed (value #t))
                              (font-size (value #t))
                              (site)))))

;;; space-frisk ends here
