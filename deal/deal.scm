;;; deal

;; Copyright (C) 2005, 2006, 2011, 2013 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: deal [--auto]
;;
;; Type ESC to quit.
;; If optional arg ‘--auto’ is omitted, you can type SPC to deal, comma
;; to warp the mouse to the "next card", period to make it blink, and
;; slash to make it and all the cards "over" it blink.  Also, you can
;; drag a card (and those covering it) around with the mouse (button 1).
;; Mouse button 3 flips the card under the mouse and all cards "over" it.

;;; Code:

(define-module (sdl-demos deal)
  #:export (main
            *files-root*
            CARD-W
            CARD-H
            SPACING
            ON-TABLE
            BACK
            C I D
            fore: seen: xrep:
            set-up-BACK!
            set-up-canvas!
            blit-C!
            baize!
            put-card!
            card-at
            rotate-selection!
            mover
            interactive-event-loop)
  #:use-module ((sdl-demos-common) #:select (datafile))
  #:use-module ((guile-baux common) #:select (fs (fso . orig-fso)
                                                 check-hv
                                                 qop<-args))
  #:use-module ((sdl sdl) #:prefix SDL:)
  #:use-module ((sdl gfx) #:prefix GFX:)
  #:use-module ((sdl simple) #:select (simple-canvas))
  #:use-module ((sdl misc-utils) #:select (copy-surface
                                           copy-rectangle
                                           rect<-surface
                                           call-with-clip-rect
                                           ignore-all-event-types-except)))

(define (fso s . args) (apply orig-fso s args) (force-output))

(define *files-root* (datafile "png"))

(define CARD-W 79)
(define CARD-H 123)
(define SPACING 18)

(define MX 20)
(define MY 20)

(define C #f)                           ; canvas

(define ON-TABLE '())

(define DECK #f)
(define BACK #f)

(define TRANSPARENT #xaa8855)           ; hmmm


(define rank: (make-object-property))
(define suit: (make-object-property))
(define seen: (make-object-property))
(define xrep: (make-object-property))
(define fore: (make-object-property))
(define back: (make-object-property))
(define ov:   (make-object-property))
(define un:   (make-object-property))
(define inbb: (make-object-property))


(define (set-up-canvas! rows cols)
  (set! C (simple-canvas #t
                         (+ (* SPACING (1+ cols)) (* CARD-W cols))
                         (+ (* SPACING (1+ rows)) (* CARD-H rows))
                         32))
  (SDL:set-caption "deal"))

(define I
  (let ((ht (make-hash-table)))
    (lambda (frag)
      (or (hashq-ref ht frag)
          (hashq-set! ht frag
                      (SDL:load-image
                       (fs "~A/~A.png"
                           *files-root*
                           (keyword->symbol frag))))))))

(define *suits* '(#:clover #:diamond #:heart #:spade))

(define B
  (let ((faces #:honors/bonded)
        (peons #:ranks/bold-09x14)
        (grand #:suits_large/knuth-15x19)
        (small #:suits_small/knuth-09x10)
        (ht (make-hash-table)))
    (define (blit-proc name wdiv hdiv xidx yidx)
      (let* ((all (I name))
             (w (/ (SDL:surface:w all) wdiv))
             (h (/ (SDL:surface:h all) hdiv))
             (srect (SDL:make-rect (* xidx w) (* yidx h) w h)))
        (lambda (dst x y)
          (let ((drect (copy-rectangle srect #:xy x y)))
            (SDL:blit-surface all srect dst drect)
            drect))))
    (define (blit-proc-face sel)
      (blit-proc faces 4 3 (list-index *suits* (cdr sel)) (- (car sel) 11)))
    (define (blit-proc-peon n)
      (blit-proc peons 14 2 (remainder n 100) (quotient n 100)))
    (define (blit-proc-suit sel)
      (blit-proc (case (car sel)
                   ((#:grand) grand)
                   ((#:small) small))
                 4 1
                 (list-index *suits* (cdr sel))
                 0))
    ;; rv
    (lambda (sel)
      (or (hashq-ref ht sel)
          (hashq-set! ht sel ((cond ((number? sel)       blit-proc-peon)
                                    ((number? (car sel)) blit-proc-face)
                                    (else                blit-proc-suit))
                              sel))))))

(define D
  (let* ((ht (make-hash-table))
         (corners (lambda (n suit)
                    (let ((shape (cons #:small suit)))
                      (and (memq suit '(#:diamond #:heart))
                           (set! n (+ 100 n)))
                      `((,n  4 6) (,n  4 103 #t)
                        (,n 66 6) (,n 66 103 #t)
                        (,shape  4 21) (,shape  4 92 #t)
                        (,shape 66 21) (,shape 66 92 #t)))))
         (peons (lambda (n suit)
                  (let ((shape (cons #:grand suit))
                        (l 15) (c 31) (r 48)
                        (t 15) (tm 38) (m 53) (bm 64) (b 90)
                        (tm3 34) (bm3 72)
                        (tm4 29) (bm4 76)
                        (acc '()))
                    (define (add! x) (set! acc (cons x acc)))
                    (define (up x y) (add! `(,shape ,x ,y)))
                    (define (dn x y) (add! `(,shape ,x ,y #t)))
                    (and (memq n '(1 3 5 9)) (up c m))
                    (and (memq n '(2 3)) (up c t) (dn c b))
                    (and (< 3 n) (up l t) (up r t) (dn l b) (dn r b))
                    (and (memq n '(6 7 8)) (up l m) (up r m))
                    (and (memq n '(7 8)) (up c tm3))
                    (and (= 8 n) (dn c bm3))
                    (and (< 8 n) (up l tm) (up r tm) (dn l bm) (dn r bm))
                    (and (= n 10) (up c tm4) (dn c bm4))
                    (reverse! acc))))
         (faces (lambda (n suit)
                  (let ((face (cons n suit)))
                    `((,face 15 15)
                      (,face 15 62 #t))))))
    (define (make-card rank suit)
      (let ((card (copy-surface (I #:blank))))
        (define (paint-element name x y . ^v)
          ;; ^v handling suboptimal (surface not hashed)
          (let ((drect ((B name) card x y)))
            (or (null? ^v)
                (SDL:blit-surface
                 (SDL:vh-flip-surface (copy-surface card drect))
                 #f card drect))))
        (define (paint ls)
          (for-each (lambda (spec)
                      (apply paint-element spec))
                    ls))
        (cond ((= 0 rank))
              (else (paint (corners rank suit))
                    (paint ((if (< 10 rank) faces peons) rank suit))))
        (set! (rank: card) rank)
        (set! (suit: card) suit)
        (set! (xrep: card) (fs "~A~A"
                               (case rank
                                 ((1) "A")
                                 ((2 3 4 5 6 7 8 9 10) rank)
                                 ((11) "J") ((12) "Q") ((13) "K")
                                 (else "?"))
                               (case suit
                                 ((#:clover) "c")
                                 ((#:diamond) "d")
                                 ((#:heart) "h")
                                 ((#:spade) "s"))))
        card))
    ;; rv
    (lambda (rank suit)
      (let ((name (list rank suit)))
        (or (hash-ref ht name)
            (hash-set! ht name (apply make-card name)))))))

(define (set-up-BACK!)
  (set! BACK (copy-surface (I #:blank)))
  (SDL:blit-surface (I #:backs/grey) #f BACK (rect<-surface BACK)))

(define (set-up-deck! count)
  (set-up-BACK!)
  (set! DECK '())
  (do ((i 0 (1+ i)))
      ((= i count))
    (for-each (lambda (suit)
                (set! DECK (append! DECK (map (lambda (i)
                                                (cons (1+ i) suit))
                                              (iota 13)))))
              *suits*)))

(define (random-card-image)
  (and (null? DECK) (set-up-deck! 1))
  (let* ((sel (list-ref DECK (random (length DECK))))
         (card (D (car sel) (cdr sel))))
    (set! DECK (delete! sel DECK))
    card))

(define (blit-C! surface dest-rect)
  (SDL:blit-surface surface #f (C) dest-rect))

(define (baize!)
  (let* ((maxw (C #:w))
         (maxh (C #:h))
         (img  (I #:baize))
         (w    (SDL:surface:w img))
         (h    (SDL:surface:h img))
         (rect (rect<-surface img)))
    (do ((x 0 (+ x w)))
        ((<= maxw x))
      (SDL:rect:set-x! rect x)
      (do ((y 0 (+ y h)))
          ((<= maxh y))
        (SDL:rect:set-y! rect y)
        (blit-C! img rect)))))

(define (inbb-proc l u)
  (let ((r (+ l CARD-W -1))
        (d (+ u CARD-H -1)))
    (lambda (x y)                       ; rv
      (and (<= l x r)
           (<= u y d)))))

(define (put-card! pc on)
  (let* ((bs-rect (SDL:make-rect 0 0 CARD-W CARD-H))
         (x  (SDL:rect:x pc))
         (y  (SDL:rect:y pc))
         (xx (+ x CARD-W -1))
         (yy (+ y CARD-H -1)))
    (set! (inbb: pc) (inbb-proc x y))
    (set! (back: pc) (let ((bs (SDL:display-format
                                (SDL:make-surface CARD-W CARD-H))))
                       (SDL:blit-surface (C) pc bs bs-rect)
                       bs))
    (set! (ov: pc) '())
    ;; check overlap
    (let loop ((ls on) (un '()))
      (if (pair? ls)
          (let* ((head (car ls))
                 (hbb? (inbb: head)))
            (if (or (hbb?  x  y)        ; ul
                    (hbb? xx  y)        ; ll
                    (hbb?  x yy)        ; ur
                    (hbb? xx yy))       ; lr
                (let ((rest (cdr ls)))
                  ;; ensure future neighborliness (part 1)
                  (define (prune x)
                    (for-each (lambda (kid)
                                (set! rest (delq kid rest))
                                (prune kid))
                              (un: x)))
                  (prune head)
                  (loop rest (cons head un)))
                (loop (cdr ls) un)))
          (let b-against-b ((ls (reverse! un)) (surviving '()))
            (if (pair? ls)
                (b-against-b
                 (cdr ls)
                 (let ((head (car ls)))
                   ;; ensure future neighborliness (part 2)
                   (if (memq head (apply append (map un: (cdr ls))))
                       surviving
                       (cons head surviving))))
                (begin
                  (for-each (lambda (s-un)
                              (set! (ov: s-un) (cons pc (ov: s-un))))
                            surviving)
                  (set! (un: pc) surviving))))))))

(define (deal!)
  (for-each (lambda (card)
              (set! (ov: card) #f)
              (set! (un: card) #f))
            ON-TABLE)
  (set! ON-TABLE '())
  (gc)
  (baize!)
  (let ((on '())
        (empty (I #:slots/plain))
        (maxw (C #:w))
        (maxh (C #:h)))
    (define (put-random-card! x y)
      (let ((pc (rect<-surface (I #:blank) x y))) ; placed card
        (set! (fore: pc) (random-card-image))
        (set! (xrep: pc) (xrep: (fore: pc)))
        (set! (seen: pc) #t)
        (put-card! pc on)
        (display " ") (display (xrep: pc))
        (or (null? (un: pc)) (display (map xrep: (un: pc))))
        (blit-C! (fore: pc) pc)
        (set! on (cons pc on))))
    (do ((x SPACING (+ x CARD-W SPACING)))
        ((<= maxw x))
      (do ((y SPACING (+ y CARD-H SPACING)))
          ((<= maxh y))
        (blit-C! empty (rect<-surface empty x y))
        (put-random-card! x y)))
    (do ((i 0 (1+ i)))
        ((= 3 i))
      (put-random-card! (random (- maxw CARD-W))
                        (random (- maxh CARD-H))))
    (set! ON-TABLE (reverse! on)))
  (SDL:flip)
  (newline))

(define (rotate-selection! count)
  (do ((i 0 (1+ i)))
      ((= i count))
    (set! ON-TABLE (append (cdr ON-TABLE) (list (car ON-TABLE))))))

(define (card-at x y . from)
  (or-map (lambda (pc)
            (and ((inbb: pc) x y)
                 (or (card-at x y (ov: pc))
                     pc)))
          (if (null? from)
              ON-TABLE
              (car from))))

(define (group-overs pc . me-too!)      ; => #(OVER REVO BB)
  (let* ((ul-x (SDL:rect:x pc))
         (ul-y (SDL:rect:y pc))
         (lr-x (+ ul-x CARD-W))
         (lr-y (+ ul-y CARD-H))
         (over '())
         (revo-box (list #f))
         (tp revo-box))

    (define (note ov)
      (or (memq ov over)
          (let ((x (SDL:rect:x ov))
                (y (SDL:rect:y ov)))
            (if (< x ul-x)
                (set! ul-x x)
                (set! lr-x (max lr-x (+ x CARD-W))))
            (if (< y ul-y)
                (set! ul-y y)
                (set! lr-y (max lr-y (+ y CARD-H))))
            (set! over (cons ov over))
            (set-cdr! tp (list ov))
            (set! tp (cdr tp))))
      ov)

    ;; do it!
    (or (null? me-too!)
        (note (car me-too!)))
    (let loop ((ls (ov: pc)))
      (or (null? ls)
          (loop (append (cdr ls) (ov: (note (car ls)))))))
    ;; rv
    (vector over
            (cdr revo-box)
            (SDL:make-rect ul-x ul-y (- lr-x ul-x) (- lr-y ul-y)))))

(define (blink-selected!)
  (let* ((head (car ON-TABLE))
         (overs (vector-ref (group-overs head) 1)))
    (define (spew! selection)
      (blit-C! (selection head) head)
      (for-each (lambda (ov)
                  (blit-C! (if (seen: ov)
                               (fore: ov)
                               BACK)
                           (copy-rectangle ov)))
                overs)
      (SDL:update-rect (C) head)
      (SDL:delay 100))
    (fso "~A ~A ~A~%"
         (map xrep: (ov: head))
         (xrep: head)
         (map xrep: (un: head)))
    (call-with-clip-rect head (lambda ()
                                (do ((i 0 (1+ i)))
                                    ((= 3 i))
                                  (spew! back:)
                                  (spew! fore:))))))

(define (blink-selected-and-overs!)
  (let* ((head (car ON-TABLE))
         (group (group-overs head head))
         (overs-hide (vector-ref group 0))
         (overs-show (vector-ref group 1))
         (bb         (vector-ref group 2)))
    (define (do-it!-proc aspect rects)
      (define (draw! pc)
        (blit-C! (aspect pc) pc))
      (lambda ()
        (for-each draw! rects)
        (SDL:update-rect (C) bb)))
    (let ((hide! (do-it!-proc back: overs-hide))
          (show! (do-it!-proc fore: overs-show)))
      (fso "~A ~A ~A~%"
           (map xrep: (ov: head))
           (xrep: head)
           (map xrep: (un: head)))
      (do ((i 0 (1+ i)))
          ((= 3 i))
        (hide!) (SDL:delay 100)
        (show!) (SDL:delay 100)))))

(define (mover pc . opts)

  (define (draw!-proc aspect)
    (lambda (pc)
      (blit-C! (if (and (eq? fore: aspect)
                        (not (seen: pc)))
                   BACK
                   (aspect pc))
               pc)))

  (define (margin rect dimension one two)
    (- (C dimension) (+ (one rect) (two rect))))

  (define (make-ambu bb ls)
    (let ((s (SDL:convert-surface
              (SDL:make-surface (SDL:rect:w bb) (SDL:rect:h bb))
              (SDL:surface-get-format (I #:blank)))))
      (SDL:fill-rect s (copy-rectangle bb #:xy 0 0) TRANSPARENT)
      (SDL:surface-color-key! s TRANSPARENT #t)
      (set! s (SDL:display-format s))
      (let ((bbx (SDL:rect:x bb))
            (bby (SDL:rect:y bb))
            (dest (SDL:make-rect 0 0 CARD-W CARD-H)))
        (for-each (lambda (pc)
                    (SDL:rect:set-x! dest (- (SDL:rect:x pc) bbx))
                    (SDL:rect:set-y! dest (- (SDL:rect:y pc) bby))
                    (SDL:blit-surface (if (seen: pc)
                                          (fore: pc)
                                          BACK)
                                      #f s dest))
                  ls))
      s))

  (let* ((serial 0)
         (tick (SDL:get-ticks))
         (tick-acc 0)
         (x-acc 0)
         (y-acc 0)
         (group (group-overs pc pc))
         (overs-hide (vector-ref group 0))
         (overs-show (vector-ref group 1))
         (bb         (vector-ref group 2))
         (bb-prev (copy-rectangle bb))
         (both-bb (list bb-prev bb))
         (draw-back! (draw!-proc back:))
         (draw-fore! (draw!-proc fore:))
         (ambu (make-ambu bb (reverse overs-hide)))
         (r-margin (margin pc #:w SDL:rect:x SDL:rect:w))
         (b-margin (margin pc #:h SDL:rect:y SDL:rect:h))
         (S (SDL:convert-surface (SDL:make-surface (C #:w) (C #:h))
                                 (SDL:surface-get-format (C)))))

    (define (move-selected-and-overs! xrel yrel)
      (let ((dx (cond ((= 0 xrel) xrel)
                      ((> 0 xrel) (let ((bbx (SDL:rect:x bb)))
                                    (if (> 0 (+ bbx xrel))
                                        (- bbx)
                                        xrel)))
                      (else (min xrel r-margin))))
            (dy (cond ((= 0 yrel) yrel)
                      ((> 0 yrel) (let ((bby (SDL:rect:y bb)))
                                    (if (> 0 (+ bby yrel))
                                        (- bby)
                                        yrel)))
                      (else (min yrel b-margin)))))
        (cond ((= 1 serial)
               (for-each draw-back! overs-hide)
               (SDL:blit-surface (C) bb S bb)))
        ;; hide
        (SDL:set-clip-rect! (C) bb)
        (SDL:blit-surface S #f (C) #f)
        (SDL:set-clip-rect! (C))
        ;; move
        (SDL:rect:set-w! bb-prev (SDL:rect:w bb))
        (SDL:rect:set-h! bb-prev (SDL:rect:h bb))
        (SDL:rect:set-x! bb-prev (SDL:rect:x bb))
        (SDL:rect:set-y! bb-prev (SDL:rect:y bb))
        (for-each (lambda (r)
                    (SDL:rect:set-x! r (+ (SDL:rect:x r) dx))
                    (SDL:rect:set-y! r (+ (SDL:rect:y r) dy)))
                  (cons bb overs-show))
        (set! r-margin (- r-margin dx))
        (set! b-margin (- b-margin dy))
        ;; show
        (SDL:set-clip-rect! S bb)
        (SDL:blit-surface (C) #f S #f)
        (SDL:set-clip-rect! S)
        (blit-C! ambu bb)
        ;; update screen
        (SDL:update-rects (C) both-bb)))

    (define (move! xrel yrel)
      (set! serial (1+ serial))
      (let* ((new-tick (SDL:get-ticks))
             (diff (- new-tick tick)))
        (set! tick new-tick)
        (set! tick-acc (+ tick-acc diff))
        (cond ((and (not (= 1 serial))
                    (or (< tick-acc 40) ; 25 fps
                        (= 1 (SDL:evqueue-peek 1 'mouse-motion))))
               (set! x-acc (+ x-acc xrel))
               (set! y-acc (+ y-acc yrel)))
              (else
               (move-selected-and-overs! (+ x-acc xrel) (+ y-acc yrel))
               (set! tick-acc 0)
               (set! x-acc 0)
               (set! y-acc 0)))))

    (define (done!)
      ;; kludge
      (cond ((> 1 serial)
             (set! serial 1)
             (move-selected-and-overs! 0 0)))
      (SDL:blit-surface S bb (C) bb)
      (for-each (lambda (pc)
                  (set! ON-TABLE (delq pc ON-TABLE))
                  (for-each (lambda (un)
                              (set! (ov: un)
                                    (delq pc (ov: un))))
                            (un: pc)))
                overs-show)
      (for-each (lambda (pc)
                  (put-card! pc ON-TABLE)
                  (draw-fore! pc)
                  (set! ON-TABLE (cons pc ON-TABLE)))
                overs-show)
      ;; keep a flat(ish) memory usage profile
      (set! ambu #f)
      (set! S #f)
      (gc))

    ;; spew (maybe)
    (or (memq #:nospew opts)
        (fso "~A (~A over)~%"
             (map xrep: overs-show)
             (1- (length overs-hide))))
    ;; rv
    (lambda (command . args)
      (apply (case command
               ((#:move!) move!)
               ((#:done!) done!))
             args))))

(define (viz-looping!)
  (ignore-all-event-types-except 'key-down)
  (deal!)
  (let ((ev (SDL:make-event))
        (fm (GFX:make-fps-manager 60)))
    (let loop ((what-to-do (random 100)))
      (GFX:fps-manager-delay! fm)
      (cond ((< 50 what-to-do)
             (rotate-selection! (1+ (random 42)))
             ((list-ref (list blink-selected!
                              blink-selected-and-overs!)
                        (random 2))))
            ((< 10 what-to-do)
             (let ((mc (mover (car ON-TABLE))))
               (do ((i 0 (1+ i)))
                   ((= i 17))
                 (GFX:fps-manager-delay! fm)
                 (mc #:move! (- (random 17) 8) (- (random 17) 8)))
               (mc #:done!)))
            (else
             (deal!)))
      (and (SDL:poll-event ev)          ; set by side effect
           (eq? 'escape (SDL:event:key:keysym:sym ev))
           (set! ev #f))
      (and ev (loop (random 100)))))
  (SDL:quit))

(define (interactive-event-loop wait-thunk handlers)
  (ignore-all-event-types-except 'mouse-button-down
                                 'mouse-button-up
                                 'mouse-motion
                                 'key-down)
  (let ((ev (SDL:make-event)))
    (let loop ()
      (wait-thunk)
      (and (SDL:wait-event ev)
           (and=> (assq-ref handlers (SDL:event:type ev))
                  (lambda (proc)
                    (set! ev (and (proc ev) ev)))))
      (and ev (loop))))
  (SDL:quit))

;; Up through Guile-SDL 0.4.3 mouse buttons are numeric.
;; After that, they are symbolic.
(define (mb-L? ev) (memq (SDL:event:button:button ev) '(1 left)))
(define (mb-R? ev) (memq (SDL:event:button:button ev) '(3 right)))

(define (viz-interactive!)
  (deal!)
  (let ((card-under-mouse #f)
        (moved? #f)
        (mc #f))                                ; move control
    (define (jam-mouse! x y)
      (set! MX (max x 0))
      (set! MY (max y 0))
      (set! moved? #t))
    (define (flip-card-and-overs pc)
      (for-each (lambda (pc)
                  (set! (seen: pc) (not (seen: pc))))
                (vector-ref (group-overs pc pc) 0))
      (set! mc (mover pc))
      (mc #:done!)
      (set! mc #f))
    (interactive-event-loop
     (lambda ()
       (cond (moved? (SDL:warp-mouse MX MY) (set! moved? #f))))
     `((mouse-button-up
        . ,(lambda (ev)
             (cond ((not (mb-L? ev)))
                   (mc (mc #:done!) (set! mc #f)))))
       (mouse-button-down
        . ,(lambda (ev)
             (cond ((and (mb-R? ev)
                         (card-at (SDL:event:motion:x ev)
                                  (SDL:event:motion:y ev)))
                    => flip-card-and-overs)
                   ((not (mb-L? ev)))
                   ((card-at (SDL:event:motion:x ev) (SDL:event:motion:y ev))
                    => (lambda (pc)
                         (let loop ()
                           (or (eq? pc (car ON-TABLE))
                               (begin
                                 (rotate-selection! 1)
                                 (loop))))
                         (set! mc (mover pc))))
                   (else
                    (fso "(nothing there)~%")))))
       (mouse-motion
        . ,(lambda (ev)
             (set! MX (SDL:event:motion:x ev))
             (set! MY (SDL:event:motion:y ev))
             (cond (mc
                    (mc #:move!
                        (SDL:event:motion:xrel ev)
                        (SDL:event:motion:yrel ev)))
                   ((card-at MX MY)
                    => (lambda (pc)
                         (or (eq? card-under-mouse pc)
                             (begin
                               (fso "~A~%" (xrep: pc))
                               (set! card-under-mouse pc)))))
                   (else
                    (set! card-under-mouse #f)))))
       (key-down
        . ,(lambda (ev)
             (case (SDL:event:key:keysym:sym ev)
               ((space) (deal!))
               ((semicolon) (let ((pc (car ON-TABLE)))
                              (or (not (eq? card-under-mouse pc))
                                  (flip-card-and-overs pc))))
               ((comma)
                (rotate-selection! 1)
                (let ((pc (car ON-TABLE)))
                  (jam-mouse! (+ 38 (SDL:rect:x pc))
                              (+  5 (SDL:rect:y pc)))))
               ((period) (blink-selected!))
               ((slash)  (blink-selected-and-overs!))
               ((right) (jam-mouse! (+ MX 20) MY))
               ((left)  (jam-mouse! (- MX 20) MY))
               ((down)  (jam-mouse! MX (+ MY 20)))
               ((up)    (jam-mouse! MX (- MY 20)))
               ((escape) #f))))))))

(define (main args)
  (define (main/qop qop)
    (set! *random-state* (let ((now (gettimeofday)))
                           (seed->random-state (* (car now)
                                                  (cdr now)))))
    (set-up-canvas! 3 2)
    (set-up-deck! 1)
    ((if (qop 'auto)
         viz-looping!
         viz-interactive!)))
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.10")
                   (help . commentary)))
  (main/qop (qop<-args args '((auto)))))

;;; deal ends here
