# autogen.sh
# Usage: sh autogen.sh

set -ex
test -d bx || mkdir bx
guile-baux-tool import common frisker forms-from
autoconf

# autogen.sh ends here
