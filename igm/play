#!/bin/sh
exec ${GUILE-guile} -L '../bx' -e main -s $0 "$@" # -*- scheme -*-
!#
;;; play

;; Copyright (C) 2013 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: play FILE...
;;
;; For each FILE, read FILE and FILE.metrics and display
;; the glyphs randomly, repeatedly (but not infinitely).

;;; Code:

(use-modules
 ((guile-baux common) #:select (fso check-hv))
 ((guile-baux forms-from) #:select (forms<-file))
 ((srfi srfi-1) #:select (span!))
 ((srfi srfi-4) #:select (list->u16vector
                          u16vector->list
                          u16vector-ref))
 ((sdl sdl) #:prefix SDL:)
 ((sdl simple) #:selet (simple-canvas)))

(define (play surface min-c max-c all-x all-w)

  (define (ni n)                        ; normalized index
    (- n min-c))

  (fso " (c) ~S ~S (x) ~S ~S (w) ~S ~S~%"
       min-c max-c
       (apply min all-x)
       (apply max all-x)
       (apply min all-w)
       (apply max all-w))
  (set! all-x (list->u16vector all-x))
  (set! all-w (list->u16vector all-w))
  (let* ((gw (+ 100 (* 100 (random 9))))
         (gh (+ 100 (* 100 (random 4))))
         (screen ((simple-canvas #t gw gh 32)))
         (h (1- (SDL:surface:h surface)))
         (srect (SDL:make-rect 0 1 0 h))
         (drect (SDL:make-rect 0 1 0 h)))
    (let repeat ((i 9999))
      (or (zero? i)
          (let* ((c (+ min-c (random (- max-c min-c))))
                 (x (u16vector-ref all-x (ni c)))
                 (w (u16vector-ref all-w (ni c))))
            (cond ((zero? x)
                   (fso "rejecting: ~S~%" c))
                  ((positive? x)
                   (SDL:rect:set-x! srect x)
                   (SDL:rect:set-w! srect w)
                   (SDL:rect:set-x! drect (random (- gw w)))
                   (SDL:rect:set-y! drect (random (- gh h)))
                   (SDL:rect:set-w! drect w)
                   (SDL:blit-surface surface srect screen drect)
                   (SDL:update-rect screen drect)))
            (repeat (1- i)))))))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.0")
                   (help . commentary)))
  (SDL:init 'video)
  (for-each (lambda (filename)
              (fso "filename: ~A~%" filename)
              (let ((metr (string-append filename ".metrics")))
                (if (file-exists? metr)
                    (apply play (SDL:load-image filename)
                           (forms<-file metr))
                    (fso " skipping (no ~A)~%" metr))))
            (cdr args))
  (SDL:delay 2000)
  (exit (SDL:quit)))

;;; Local variables:
;;; compile-command: "./play data/*.png"
;;; End:

;;; play ends here
