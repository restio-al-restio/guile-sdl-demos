;;; igm --- idle glued moss!

;; Copyright (C) 2013 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: igm [options]
;;
;; Options are:
;;  -n, --narcissist     -- display abbreviated forms from
;;                          programs grok, play and igm
;;  -f, --font NICK      -- use data/NICK.png [bigfont3]
;;
;; Display some text lines using the "surface font" (aka "sfont")
;; files in data/, repeatedly.  Lines are animated (slightly).
;;
;; Normally the text lines are taken from a builtin set.
;; Option ‘--narcissist’ (short form: ‘-n’) means to mine
;; ‘define’, ‘define-module’ and named-‘let’ forms from the
;; programs in this demo, and display them (abbreviated)
;; instead.
;;
;; Type ESC to quit.

;;; Code:

(define-module (sdl-demos igm)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile))
  #:use-module ((guile-baux common) #:select (check-hv
                                              qop<-args
                                              fs))
  #:use-module ((guile-baux forms-from) #:select (forms<-file))
  #:use-module ((srfi srfi-1) #:select (filter
                                        find
                                        car+cdr
                                        append-map))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((srfi srfi-4) #:select (list->u16vector
                                        u16vector-ref))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-14) #:select (char-set-contains?
                                         char-set:ascii
                                         char-set:letter
                                         char-set:digit
                                         char-set-intersection))
  #:use-module ((sdl sdl) #:prefix S/)
  #:use-module ((sdl gfx) #:prefix G/)
  #:use-module ((sdl misc-utils) #:select (ignore-all-event-types-except
                                           exact-truncate
                                           rect<-surface
                                           copy-rectangle))
  #:use-module ((sdl simple) #:select (simple-canvas)))

(define GEOM-W 800)
(define GEOM-H 600)

(define SCREEN #f)

(define FOCUSFX #f)
(define FOCUSFX-W #f)

(define (half n)
  (ash n -1))

(define (staging-surface w h)
  (let ((surf (S/make-surface w h)))
    (S/surface-color-key! surf 0 #t)
    surf))

(define (font nick)

  (define (filename . suffix)
    (datafile (fs "data/~A.png~A" nick (if (pair? suffix)
                                           (car suffix)
                                           ""))))

  (define (norm min-c max-c all-x all-w)
    (values (S/load-image (filename))
            min-c max-c
            (list->u16vector all-x)
            (list->u16vector all-w)))

  (let-values (((src min-c max-c all-x all-w) (apply norm (forms<-file
                                                           (filename
                                                            ".metrics")))))

    (define (uv-ref uvec n)
      (u16vector-ref uvec (- n min-c)))

    (define (width n)
      (or (and (<= min-c n max-c)
               (uv-ref all-w n))
          0))

    (let* ((h (1- (S/surface:h src)))
           (h/2 (half h))
           (blank-width (half (width (char->integer #\0))))
           (srect (S/make-rect 0 1 0 h)))

      (define (surface<-n n)
        (let* ((w (width n))
               (surf (staging-surface w h)))
          (cblit! surf n (half w) (half h))
          surf))

      (define (effective-width n)
        (let ((w (width n)))
          (if (zero? w)
              blank-width
              w)))

      (define (cblit! dst n cx cy)
        (and-let* ((w (width n))
                   ((positive? w)))
          (S/rect:set-x! srect (uv-ref all-x n))
          (S/rect:set-w! srect w)
          (S/blit-surface src srect
                          dst (copy-rectangle
                               srect #:xy
                               (- cx (half w))
                               (- cy h/2)))))

      (define (metrics ls pad)
        (let loop ((ls ls)
                   (start 0)
                   (acc-x '()))
          (if (null? ls)
              (values start
                      (reverse! acc-x))
              (let ((w (effective-width (car ls))))
                (loop (cdr ls)
                      (+ start w (if (null? (cdr ls))
                                     0
                                     pad))
                      (cons (+ start (half w))
                            acc-x))))))

      (define (ls-ren ls pad)
        (let-values (((w cx) (metrics ls pad)))
          (let ((surf (staging-surface w h)))

            (define (c-ren! n cx)
              (cblit! surf n cx h/2))

            (for-each c-ren! ls cx)
            surf)))

      (values h
              surface<-n
              blank-width
              ls-ren))))

(define relevant
  (let ((ok (char-set-intersection char-set:ascii
                                   char-set:letter)))

    (define (interesting pair)
      (let ((c (car pair)))
        (or (char-set-contains? char-set:digit c)
            (char-set-contains? ok c))))

    ;; relevant
    (lambda (chars)
      (filter interesting
              (map cons
                   chars
                   (iota (length chars)))))))

(define (flipped-case c)
  ((if (char-upper-case? c)
       char-downcase
       char-upcase)
   c))

(define (same-ish wc)
  (let ((fc (flipped-case wc)))

    (define (hit pair)
      (let ((nc (car pair)))
        (or (char=? wc nc)
            (char=? fc nc))))

    hit))

(define (detect-anagram was now)
  (let ((wlen (length was))
        (nlen (length now))
        (rwas (relevant was))
        (rnow (relevant now)))
    (and (pair? rwas)
         (pair? rnow)
         (= (length rwas)
            (length rnow))
         (let loop ((acc '())
                    (was rwas)
                    ;; reverse to mix it up a bit
                    (now (reverse rnow)))
           (if (pair? was)
               (let-values (((wc idx) (car+cdr (car was))))
                 (cond ((find (same-ish wc) now)
                        => (lambda (to)
                             (loop (cons (list wc (car to) idx (cdr to))
                                         acc)
                                   (cdr was)
                                   (delq to now))))
                       (else
                        ;; failure! (no hit) rv
                        #f)))
               ;; success! rv
               (list wlen rwas
                     nlen rnow
                     (reverse! acc)))))))

(define eye-the-door-and-leave-maybe!
  (let ((ev (S/make-event)))
    ;; eye-the-door-and-leave-maybe!
    (lambda ()
      (and (S/poll-event ev)
           (eq? 'key-down (S/event:type ev))
           (eq? 'escape (S/event:key:keysym:sym ev))
           (quit)))))

(define PI (* 4 (atan 1.0)))

(define (animate nick all)

  (define lucky-duck
    (let* ((v (list->vector all))
           (count (vector-length v)))
      ;; lucky-duck
      (lambda ()
        (string->list (vector-ref v (random count))))))

  (ignore-all-event-types-except 'key-down)
  (let* ((clear (S/make-rect 0 0 GEOM-W 0))
         (fx-h (S/surface:h FOCUSFX))
         (fx-sr (rect<-surface FOCUSFX))
         (fx-at (rect<-surface FOCUSFX)))

    (define (bg!)
      (S/blit-surface FOCUSFX fx-sr SCREEN fx-at))

    (let-values (((font-height surface<-n blank-width ls-ren) (font nick)))
      (let loop ((prev #f) (prev-text #f))
        (let* ((one (lucky-duck))
               (ana (and prev (detect-anagram prev one)))
               (yep (G/make-fps-manager 120))
               (ls (map char->integer one))
               (fx-y (if ana
                         (S/rect:y fx-at) ; don't move
                         (random (- GEOM-H fx-h))))
               (y (+ fx-y
                     (half fx-h)
                     (- (half font-height))))
               (text #f)
               (at (S/make-rect 0 y 1 font-height)))

          (define (set-at! surf)
            (let ((w (S/surface:w surf)))
              (S/rect:set-x! at (half (- GEOM-W w)))
              (S/rect:set-w! at w)))

          (define (settle!)
            (eye-the-door-and-leave-maybe!)
            (S/update-rect SCREEN fx-at)
            (S/rect:set-x! fx-sr (modulo (1+ (S/rect:x fx-sr))
                                         FOCUSFX-W))
            (G/fps-manager-delay! yep))

          (define (updn steps)
            (or (null? steps)
                (let* ((step (car steps))
                       (y (+ (S/rect:y fx-at)
                             step)))
                  (S/rect:set-h! clear (abs step))
                  (S/rect:set-y! clear (if (negative? step)
                                           (+ y fx-h)
                                           (S/rect:y fx-at)))
                  (S/fill-rect SCREEN clear 0)
                  (S/update-rect SCREEN clear)
                  (S/rect:set-y! fx-at y)
                  (bg!)
                  (settle!)
                  (updn (cdr steps)))))

          (define (shuffle wlen was nlen now change)

            (define (fluff len ls)
              (let ((v (make-vector len #\space)))
                (for-each (lambda (pair)
                            (vector-set! v (cdr pair) (car pair)))
                          ls)
                (map char->integer (vector->list v))))

            (define (in/out len ls start ch end)
              (let ((ls (fluff len ls)))

                (define (fg! pad)
                  (set! prev-text (ls-ren ls pad))
                  (set-at! prev-text)
                  (S/blit-surface prev-text #f SCREEN at))

                (do ((i start (ch i)))
                    ((= end i))
                  (bg!)
                  (fg! i)
                  (settle!))))

            (define (x-of count)
              (let* ((between (* 3 blank-width))
                     (width (* between (1- count)))
                     (left (half (- GEOM-W width))))
                ;; x-of
                (lambda (i)
                  (+ left (* between i)))))

            (in/out wlen was 0 1+ blank-width)
            (let* ((total-ticks 420)
                   (headroom (- (half (S/rect:h fx-at))
                                (half font-height)))
                   (beg-x (x-of wlen))
                   (end-x (x-of nlen))
                   (base (+ (S/rect:y fx-at)
                            headroom)))

              (define (surf c)
                (surface<-n (char->integer c)))

              (define (layout wc nc wi ni)
                (let* ((beg (surf wc))
                       (end (surf nc))
                       (beg-x (beg-x wi))
                       (end-x (end-x ni))
                       (diff (- end-x beg-x))
                       (speed (* PI (- (random 19) 9)))
                       (amp (* headroom (+ 0.1 (/ (random 90) 100))))
                       (t1 (random total-ticks))
                       (t2 (+ t1 (random (- total-ticks t1)))))
                  ;; rv
                  (lambda (tick)
                    (let* ((cur (cond ((>= t1 tick) beg)
                                      ((>= t2 tick) (if (even? tick)
                                                        beg
                                                        end))
                                      (else end)))
                           (pct (/ tick total-ticks))
                           (cx (exact-truncate
                                (+ (* diff pct)
                                   beg-x)))
                           (cy (exact-truncate
                                (+ (* amp (sin (* speed pct)))
                                   base)))
                           (drect (rect<-surface cur cx cy)))
                      (S/blit-surface cur #f SCREEN drect)))))

              (let ((in-motion (list->vector
                                (map (lambda (spec)
                                       (apply layout spec))
                                     change))))

                (define (spin tick)
                  (bg!)
                  (array-for-each
                   (lambda (single)
                     (single tick))
                   in-motion)
                  (settle!))

                (define (hover dally tick)
                  (do ((i 0 (1+ i)))
                      ((= dally i))
                    (spin tick)))

                ;; do it!
                (hover 42 0)
                (do ((tick 0 (1+ tick)))
                    ((= total-ticks tick))
                  (spin tick))
                (hover 88 total-ticks)))
            (in/out nlen now (1- blank-width) 1- -1))

          (or (not prev)
              (and ana (apply shuffle ana))
              (let* ((old-y (S/rect:y fx-at))
                     (diff (- fx-y old-y))
                     (norm (quotient diff 15)))
                (updn (delq 0 (cons (+ norm (remainder diff 15))
                                    (make-list 14 norm))))))

          (S/rect:set-y! fx-at fx-y)
          (let spin ((frame (if ana 0 -42)))

            (define (ok)
              (let ((pad (if (> 120 frame)
                             ;; inhale
                             (- (ash frame
                                     ;; slowly :-D
                                     -2)
                                5)
                             ;; exhale
                             (- 145 frame))))
                (set! text (ls-ren ls pad))
                (set-at! text)))

            (define (etc)
              (and text (S/blit-surface text #f SCREEN at))
              (settle!)
              (spin (1+ frame)))

            (bg!)
            (cond ((>= 0   frame)      (etc))
                  ((>= 145 frame) (ok) (etc))
                  ((>= 256 frame)      (etc))
                  (else (loop one text)))))))))

(define (me-myself-and-i)

  (define (interesting form)
    (and (pair? form)
         (or (memq (car form) '(define define-module))
             (and (eq? 'let (car form))
                  (pair? (cdr form))
                  (symbol? (cadr form))))))

  (define (from program)
    (forms<-file (datafile program)))

  (let* ((all (append-map from '("grok"
                                 "play"
                                 "igm")))
         (defs (filter interesting all))
         (sub '()))

    (define (trundle x)
      (and (interesting x)
           (set! sub (cons x sub)))
      (cond ((and (pair? x)
                  (not (eq? 'quote (car x))))
             (trundle (car x))
             (trundle (cdr x)))))

    (define (nicely form)
      (set-cdr! (cdr form) '(...))
      (fs "~S" form))

    (for-each trundle (map cddr defs))
    (map nicely (append sub defs))))

(define (main/qop qop)
  (set! SCREEN ((simple-canvas #f GEOM-W GEOM-H 32)))
  (set! FOCUSFX
        (let* ((one (S/load-image (datafile "data/focusfx.PNG")))
               (eno (S/vertical-flip-surface one))
               (at (rect<-surface one))
               (w (S/surface:w one))
               (h (S/surface:h one))
               (all-w (+ GEOM-W w))
               (all (S/make-surface all-w (* 2 h))))
          (set! FOCUSFX-W w)
          (do ((x (- (half GEOM-W) (* w (quotient GEOM-W w)))
                  (+ w x)))
              ((< all-w x))
            (S/rect:set-x! at x)
            (S/rect:set-y! at 0)
            (S/blit-surface eno #f all at)
            (S/rect:set-x! at (+ x (half w)))
            (S/rect:set-y! at h)
            (S/blit-surface one #f all at))
          all))
  (let ((font (or (qop 'font)
                  'bigfont3))
        (all (if (qop 'narcissist)
                 (me-myself-and-i)
                 '("Guile-SDL Demos"
                   "IDLE GLUED MOSS"
                   "Odd is Mel's glue!"
                   "Kerouac leered thus..."
                   "Read the Source, Luke!"
                   "eureka SOUL retched"
                   ;; testing
                   "\"To be or not to be?\""
                   "THAT is the question!"
                   "42"
                   "8675309"
                   "<-=|!*., M&U ,.*!|=->"
                   "/\\@#$%^&_+;:'"
                   ))))

    (define (!walk)
      (animate font all))

    (define (!rest . unimportant-details)
      (S/quit))

    (catch 'quit
           !walk
           !rest)))

(define (main args)
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "1.1")
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((font (single-char #\f) (value #t))
           (narcissist (single-char #\n))))))

;;; igm ends here
