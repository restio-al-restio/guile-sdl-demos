;;; view-vcg

;; Copyright (C) 2004, 2005, 2006, 2011, 2012, 2013, 2021 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Usage: view-vcg [options] VCG-FILE
;;
;; Options [default values] are:
;;  -g, --geometry WxH   -- use window sized WxH [800x600]
;;  -q, --quiet          -- suppress graph info display
;;  -d, --depth N        -- display N background star planes [3]
;;  -a, --aaline         -- use anti-aliased lines to draw edges
;;
;; Keybindings: ‘e’ and ‘v’ toggle edge and vertex visibility, respectively;
;; ‘s’ increase vertex size; ‘a’ and ‘d’ rotate counter- and clockwise,
;; respectively; ‘w’ toggles wrap mode; ‘o’ toggles gravitation toward the
;; original layout; SPACE toggles motion; ESC quits.  You can use ‘j’, ‘k’,
;; and ‘l’ for ‘a’, ‘s’ and ‘d’.
;;
;; NOTE: Nodes in VCG-FILE lacking a position are assigned a random one.

;;; Code:

(define-module (view-vcg)
  #:export (main)
  #:use-module ((sdl-demos-common) #:select (datafile decry))
  #:use-module ((guile-baux common) #:select (fs (fso . >>)
                                                 check-hv qop<-args))
  #:use-module ((sdl sdl) #:prefix SDL::)
  #:use-module ((sdl gfx) #:prefix GFX::)
  #:use-module ((sdl misc-utils) #:prefix MISC::)
  #:use-module ((sdl simple) #:select (simple-canvas))
  #:use-module ((srfi srfi-8) #:select (receive)))

(define (read-vcg-file filename)
  ;; Return the internal graph representation object created
  ;; from reading VCG file @var{filename}.
  ;;
  ;; Note: This is originally from module (ttn rw-vcg), with
  ;; ‘vcg->sexp.sed’ internalized and made into a procedure.

  (define (vcg->sexp.sed)
    (fs "sed '~A~A~A~A~A~A' ~A"
        "s/\\([a-z][a-z._0-9]*\\) *:/#:\\1 /g;"
        "s/{/(/g;"
        "s/}/)/g;"
        "s/\\(classname\\|infoname\\) \\([0-9][0-9]*\\) *:/#:\\1 \\2 /g;"
        "s/\\(colorentry\\) \\([0-9][0-9]*\\) *:/#:\\1 \\2 /g;"
        "s/\\\\#/#/g"
        filename))

  (let* ((tmpfile (fs "view-vcg.~A.TMP" (let ((now (gettimeofday)))
                                          (* (car now) (cdr now)))))
         (p (if (and (zero? (system (fs "~A > ~A" (vcg->sexp.sed) tmpfile)))
                     (file-exists? tmpfile))
                (open-input-file tmpfile)
                (decry "cannot read vcg file ~A" filename))))
    (let ((first (read p)))
      (or (and (keyword? first) (eq? #:graph first))
          (decry "bad vcg file (first form not #:graph keyword): ~A"
                 filename)))
    (let ((form (read p)))
      (let loop ((ls form))             ; validate/munge in place
        (cond ((null? ls))
              ((not (keyword? (car ls)))
               (decry "vcg file ~A missing keyword, has instead ~S"
                      filename (car ls)))
              (else
               (case (car ls)
                 ((#:node)
                  (let nloop ((nls (cadr ls)))
                    (or (null? nls)
                        (case (car nls)
                          ((#:loc)
                           (let* ((orig (cadr nls))
                                  (reformed (cons (list-ref orig 1)
                                                  (list-ref orig 3)))
                                  (new-tail (cddr nls)))
                             (set-cdr! nls (cons reformed new-tail))
                             (nloop new-tail)))
                          (else
                           (nloop (cddr nls))))))
                  (loop (cddr ls)))
                 ((#:edge)
                  ;; (clean-edge! (cdr ls))
                  (loop (cddr ls)))
                 ((#:classname #:infoname)
                  (let* ((last-to-be-collected (list-cdr-ref ls 2))
                         (new-tail (cdr last-to-be-collected)))
                    (set-cdr! last-to-be-collected '())
                    (set-cdr! ls (cons (cdr ls) new-tail)) ; raise int-string
                    (loop new-tail)))
                 ((#:colorentry)
                  (let* ((last-to-be-collected (list-cdr-ref ls 4))
                         (new-tail (cdr last-to-be-collected)))
                    (set-cdr! last-to-be-collected '())
                    (set-cdr! ls (cons (cdr ls) new-tail)) ; raise i-r-g-b
                    (loop new-tail)))
                 (else
                  ;; va bene
                  (loop (cddr ls)))))))
      (close-port p)
      (delete-file tmpfile)
      form)))

(define get object-property)
(define put set-object-property!)

(define xrep (make-object-property))

(define (sexp->ir sexp)
  (let ((attributes '())                ; alist
        (nodes '())                     ; title w/ prop
        (edges '()))                    ; (SOURCE . TARGET) w/ prop

    (define (stash-attribute! ls)
      (let* ((value (cadr ls))
             (attr (cons (car ls) value)))
        (set! attributes (cons attr attributes))))

    (define (stash-node! ls)
      (let* ((node-info (cadr ls))
             (name (cadr (memq #:title node-info)))
             (title (string->symbol name)))
        (set! (xrep title) name)
        (put title 'node-info node-info)
        (set! nodes (cons title nodes))))

    (define (stash-edge! ls)
      (let* ((edge-info (cadr ls))
             (sourcename (cadr (memq #:sourcename edge-info)))
             (targetname (cadr (memq #:targetname edge-info)))
             (edge (cons (string->symbol sourcename)
                         (string->symbol targetname))))
        (set! (xrep edge) (cons sourcename targetname))
        (put edge 'edge-info edge-info)
        (set! edges (cons edge edges))))

    ;; collect
    (let loop ((ls sexp))
      (or (null? ls)
          (begin
            ((case (car ls)
               ((#:node #:nearnode) stash-node!)
               ((#:edge #:nearedge) stash-edge!)
               (else stash-attribute!))
             ls)
            (loop (cddr ls)))))

    ;; wrap it all up
    (vector (reverse attributes) nodes edges)))

(define (vcg:attrs vcg) (vector-ref vcg 0))
(define (vcg:nodes vcg) (vector-ref vcg 1))
(define (vcg:edges vcg) (vector-ref vcg 2))

(define (find-min-max vcg xdomain ydomain)
  (let ((x-min 999999) (y-min 999999) (x-max -999999) (y-max -999999))
    (define (stretch-x x)
      (set! x-min (min x-min x))
      (set! x-max (max x-max x)))
    (define (stretch-y y)
      (set! y-min (min y-min y))
      (set! y-max (max y-max y)))
    (for-each (lambda (title)
                (and=> (or (and=> (memq #:loc (get title 'node-info)) cadr)
                           (cons (random xdomain) (random ydomain)))
                       (lambda (x-y)
                         (put title #:orig-loc (copy-tree x-y))
                         (put title #:loc x-y)
                         (stretch-x (car x-y))
                         (stretch-y (cdr x-y)))))
              (vcg:nodes vcg))
    (values x-min y-min x-max y-max)))

(define (fit! vcg new-x new-y)
  (let ((attributes (vcg:attrs vcg)))
    (and=> (assq #:x attributes)
           (lambda (cell) (set-cdr! cell (new-x (cdr cell)))))
    (and=> (assq #:y attributes)
           (lambda (cell) (set-cdr! cell (new-y (cdr cell))))))
  (for-each (lambda (title)
              (and=> (get title #:loc)
                     (lambda (x-y)
                       (let ((nx (new-x (car x-y)))
                             (ny (new-y (cdr x-y)))
                             (orig (get title #:orig-loc)))
                         (set-car! x-y nx)
                         (set-cdr! x-y ny)
                         (set-car! orig nx)
                         (set-cdr! orig ny)))))
            (vcg:nodes vcg)))

(define (display-vcg vcg)               ; to stdout
  (let ((show (lambda (extract name . extra)
                (>> "~A:~%" name)
                (for-each (lambda (x)
                            (>> " ~S" (or (xrep x) x))
                            (if (null? extra)
                                (newline)
                                (>> " ~S~%" ((car extra) x))))
                          (extract vcg)))))
    (show vcg:attrs "attributes")
    (show vcg:nodes "nodes" (lambda (title)
                              (and=> (memq #:loc (get title 'node-info))
                                     cadr)))
    (show vcg:edges "edges")))

(define int<- MISC::exact-truncate)

(define (int-/ x y)
  (int<- (/ x y)))

(define (background-mangler dest depth)
  (let ((w (SDL::surface:w dest))
        (h (SDL::surface:h dest))
        (indices (iota depth)))

    (define (background idx)
      (let* ((radius (1+ (* 2 idx)))
             (surface (SDL::make-surface w h '(src-colorkey
                                               rle-accel)))
             (enough (int-/ (* w h) (* 42 42 radius))))
        (SDL::surface-color-key! surface 0)
        (do ((i 0 (1+ i)))
            ((= enough i))
          (GFX::draw-circle surface (random w) (random h)
                            (if (zero? (random 2))
                                radius
                                (1- radius))
                            (let ((limit (ash 1 (if (zero? (random 10))
                                                    ;; balkingly brilliant
                                                    8
                                                    ;; subtly spaceworthy
                                                    6))))
                              (logior (ash (random limit) 24)
                                      (ash (random limit) 16)
                                      (ash (random limit)  8)
                                      #xff))
                            (zero? (random 2))))
        surface))

    (let ((planes (map background indices))
          (origins (map (lambda (ignored)
                          (cons 0 0))
                        indices))
          (dx #f)
          (dy #f))

      (define (turn!)

        (define (offset)
          (- (random 7) 3))

        (define (mult delta)
          (lambda (idx)
            (* (1+ idx) delta)))

        (let loop ()
          (let ((ndx (offset))
                (ndy (offset)))
            (cond ((and (zero? ndx)
                        (zero? ndy))
                   ;; struck dumb by no sum!
                   (loop))
                  (else
                   (set! dx (map (mult ndx) indices))
                   (set! dy (map (mult ndy) indices)))))))

      (define (pan-and-draw-plane! plane origin dx dy)

        (define (maybe ex ey x y w h)
          (and (not (or ex ey))
               (SDL::make-rect x y w h)))

        (define (blit! from x y)
          (SDL::blit-surface plane from dest (SDL::make-rect x y 0 0)))

        (let* ((ox (modulo (+ (car origin) dx) w))
               (oy (modulo (+ (cdr origin) dy) h))
               (eL? (zero? ox)) (eR? (= (1- w) ox))
               (eT? (zero? oy)) (eB? (= (1- h) oy))
               (ul (maybe eL? eT?  0  0  (1- ox)  (1- oy)))
               (ur (maybe eR? eT? ox  0 (- w ox)  (1- oy)))
               (ll (maybe eL? eB?  0 oy  (1- ox) (- h oy)))
               (lr (maybe eR? eB? ox oy (- w ox) (- h oy))))
          (set-car! origin ox)
          (set-cdr! origin oy)
          (and ul (blit! ul (- w ox -1) (- h oy -1)))
          (and ur (blit! ur       0     (- h oy -1)))
          (and ll (blit! ll (- w ox -1)       0))
          (and lr (blit! lr       0           0))))

      (define (draw!)
        (for-each (lambda (plane origin dx dy)
                    (pan-and-draw-plane! plane origin dx dy))
                  planes
                  origins
                  dx
                  dy))

      ;; rv
      (lambda (command)
        ((case command
           ((#:draw!) draw!)
           ((#:turn!) turn!)))))))

(define (make-SDL-manager caption geom vcg starfield-depth draw-line)
  (let* ((canvas (simple-canvas #t (car geom) (cdr geom) 32))
         (c-surf (canvas))
         (stars (background-mangler c-surf starfield-depth))

         (xofs #f) (yofs #f) (V #f)
         (bloc #f)                      ; blit location (rectangle)

         (edge-color #f)

         (wrap? #f))

    (define (meander!)
      (stars #:turn!))

    (define new-V!
      (let ((ORIG #f) (sz 0.2) (angle 0))
        (lambda (how)
          (or ORIG
              (set! ORIG (SDL::display-format (SDL::load-image
                                               (datafile "V.xpm")))))
          (and (eq? #t how)
               (set! how (vector-ref '#(#:rotate-clockwise
                                        #:rotate-counter-clockwise
                                        #:size #:size #:size)
                                     (random 5))))
          (case how
            ((#:rotate-clockwise) (set! angle (+ angle 23)))
            ((#:rotate-counter-clockwise) (set! angle (- angle 23)))
            ((#:size) (set! sz (let ((new-sz (+ sz 0.1)))
                                 (if (< 1.0 new-sz)
                                     0.2
                                     new-sz)))))
          (set! V (GFX::roto-zoom-surface ORIG angle sz #t))
          (let ((new-w (SDL::surface:w V))
                (new-h (SDL::surface:h V)))
            (set! bloc (SDL::make-rect 0 0 new-w new-h))
            (set! xofs (- (int-/ new-w 2)))
            (set! yofs (- (int-/ new-h 2)))))))

    (define nodes
      (let* ((all (vcg:nodes vcg))
             (all! (lambda (proc) (for-each proc all)))
             (odiv #f))
        (define (init! title)
          (put title #:dx (- (random 19) 9))
          (put title #:dy (- (random 19) 9)))
        (define (move! title)
          (let* ((loc (get title #:loc))
                 (orig (get title #:orig-loc))
                 (dx (if odiv
                         (let ((full (- (car orig) (car loc))))
                           (if (= 0 full)
                               0
                               (let ((step (int-/ full odiv)))
                                 (if (positive? full)
                                     (max  1 step)
                                     (min -1 step)))))
                         (get title #:dx)))
                 (dy (if odiv
                         (let ((full (- (cdr orig) (cdr loc))))
                           (if (= 0 full)
                               0
                               (let ((step (int-/ full odiv)))
                                 (if (positive? full)
                                     (max  1 step)
                                     (min -1 step)))))
                         (get title #:dy)))
                 (x-new (+ dx (car loc)))
                 (y-new (+ dy (cdr loc))))
            (cond (wrap? (set! x-new (modulo x-new (1+ (canvas #:w))))
                         (set! y-new (modulo y-new (1+ (canvas #:h))))))
            (if (< -1 x-new (canvas #:w))
                (set-car! loc x-new)
                (put title #:dx (- dx)))
            (if (< -1 y-new (canvas #:h))
                (set-cdr! loc y-new)
                (put title #:dy (- dy)))))
        (define (draw! title)
          (let ((loc (get title #:loc)))
            (SDL::rect:set-x! bloc (+ (car loc) xofs))
            (SDL::rect:set-y! bloc (+ (cdr loc) yofs))
            (SDL::blit-surface V #f c-surf bloc)))
        ;; rv
        (lambda (command)
          (case command
            ((#:draw!) (all! draw!))
            ((#:move!) (all! move!))
            ((#:orig!) (set! odiv (if odiv #f
                                      (begin
                                        (all! init!)
                                        (+ 16 (random 16))))))
            ((#:init!) (all! init!))))))

    (define edges
      (let* ((all (vcg:edges vcg))
             (all! (lambda (proc) (for-each proc all))))
        (define (draw! edge)
          (let ((source (get (car edge) #:loc))
                (target (get (cdr edge) #:loc)))
            (and source target
                 (draw-line c-surf
                            (car source) (cdr source)
                            (car target) (cdr target)
                            edge-color))))
        ;; rv
        (lambda (command)
          (case command
            ((#:draw!) (all! draw!))))))

    (define (init!)
      (>> "screen: ~A~%" c-surf)
      (SDL::set-caption caption caption)
      (SDL::enable-key-repeat 10 10)
      (MISC::ignore-all-event-types-except 'key-down)
      (new-V! #f)
      (nodes #:init!))

    (define (loop!)
      (let ((pause? #f)
            (ev (SDL::make-event))
            (fm (GFX::make-fps-manager 20)))
        (let loop ()
          (or pause? (nodes #:move!))
          (and V (= 0 (random 100)) (new-V! #t))
          (GFX::fps-manager-delay! fm)
          (canvas #:clear!)
          (stars #:draw!)
          (and edge-color (edges #:draw!))
          (and V (nodes #:draw!))
          (SDL::flip)
          (and (SDL::poll-event ev)     ; set by side effect
               (case (SDL::event:key:keysym:sym ev)
                 ((escape) (set! ev #f))
                 ((e)
                  (meander!)
                  (set! edge-color
                        (if edge-color
                            #f
                            (begin
                              (set! wrap? #f)
                              (+ #xff (ash (random #xffffff) 8))))))
                 ((v)
                  (meander!)
                  (if V (set! V #f) (new-V! #f)))
                 ((s k)
                  (new-V! #:size))
                 ((a j)
                  (new-V! #:rotate-clockwise))
                 ((d l)
                  (new-V! #:rotate-counter-clockwise))
                 ((w)
                  (meander!)
                  (set! wrap? (not wrap?)))
                 ((o)
                  (meander!)
                  (nodes #:orig!))
                 ((space)
                  (meander!)
                  (set! pause? (not pause?)))))
          (and ev (loop)))))

    (define (finish!)
      (SDL::quit))

    (meander!)
    ;; rv
    (lambda (key . args)
      (or (keyword? key)
          (decry "not a keyword: ~S" key))
      (apply
       (case key
         ((#:init) init!)
         ((#:loop!) loop!)
         ((#:finish) finish!)
         (else (decry "unrecognized key: ~S" key)))
       args))))

(define (view-vcg-interactive/qop qop vcg caption)
  (let ((geom (or (qop 'geometry
                       (lambda (s)
                         (let* ((cut (string-index s #\x))
                                (w (string->number (substring s 0 cut)))
                                (h (string->number (substring s (1+ cut)))))
                           (cons w h))))
                  '(800 . 600)))
        (quiet? (qop 'quiet)))
    (or quiet? (>> "attributes: ~A~%nodes: ~A~%edges: ~A~%"
                   (length (vcg:attrs vcg))
                   (length (vcg:nodes vcg))
                   (length (vcg:edges vcg))))
    (or quiet? (display-vcg vcg))
    (receive (x-min y-min x-max y-max) (find-min-max vcg (car geom)
                                                     (cdr geom))
      (or quiet? (>> "min: (~A . ~A)~%max: (~A . ~A)~%"
                     x-min y-min x-max y-max))
      (let ((x-factor (/ (car geom) (max 1 (- x-max x-min))))
            (y-factor (/ (cdr geom) (max 1 (- y-max y-min)))))
        (or quiet? (>> "x-factor: ~A~%y-factor: ~A~%"
                       x-factor y-factor))
        (fit! vcg
              (lambda (old-x) (int<- (* x-factor (- old-x x-min))))
              (lambda (old-y) (int<- (* y-factor (- old-y y-min)))))))
    (let ((interactive (make-SDL-manager caption geom vcg
                                         (or (qop 'depth string->number) 3)
                                         (if (qop 'aaline)
                                             GFX::draw-aa-line
                                             GFX::draw-line))))
      (interactive #:init)
      (interactive #:loop!)
      (interactive #:finish))))

(define (view-vcg/qop qop)
  (let* ((filename (let ((try (qop '())))
                     (if (null? try)
                         (datafile "data/forms2.vcg")
                         (car try))))
         (vcg (sexp->ir (read-vcg-file filename))))
    (view-vcg-interactive/qop qop vcg filename)))

(define (main args)
  (set! *random-state* (seed->random-state (let ((now (gettimeofday)))
                                             (* (car now) (cdr now)))))
  (check-hv args '((package . "Guile-SDL Demos")
                   (version . "2.4")
                   ;; 2.4 -- use ‘decry’
                   ;; 2.3 -- internal changes
                   ;; 2.2 -- avoid ‘apply-to-args’
                   ;; 2.1 -- add background stars, add ‘--depth’
                   ;; 2.0 -- drop ‘--batch’, inline rw-vcg.scm, etc
                   ;; 1.3 -- use ‘primitive-load’, misc slog
                   ;; 1.2 -- assigned position, more robust ‘read-vcg-file’
                   ;; 1.1 -- support ‘--aaline’
                   ;; 1.0 -- basic
                   (help . commentary)))
  (view-vcg/qop
   (qop<-args args '((aaline   (single-char #\a))
                     (quiet    (single-char #\q))
                     (depth    (single-char #\d) (value #t))
                     (geometry (single-char #\g) (value #t))))))

;;; view-vcg ends here
