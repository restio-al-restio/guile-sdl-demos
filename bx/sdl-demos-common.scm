;;; sdl-demos-common.scm

;; Copyright (C) 2011, 2021 Thien-Thi Nguyen
;;
;; Guile-SDL Demos is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; Guile-SDL Demos is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Code:

(define-module (sdl-demos-common)
  #:export (datafile
            decry))

(define datafile
  (let ((program-dir (dirname (car (command-line)))))
    (lambda (relative-filename)
      (in-vicinity program-dir relative-filename))))

(define (decry s . args)
  (apply format
         (current-error-port)
         (string-append (car (command-line))
                        ": ERROR: "
                        s
                        "~%")
         args)
  (exit #f))

;;; sdl-demos-common.scm ends here
